//
//  PlayerPlaylistCell.h
//  Download333
//
//  Created by Aphinop Supawaree on 1/2/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerPlaylistCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imgThmcover;
@property (retain, nonatomic) IBOutlet UILabel *lblSongName;
@property (retain, nonatomic) IBOutlet UILabel *lblArtistName;
@property (retain, nonatomic) IBOutlet UILabel *lblAlbumName;
@property (retain, nonatomic) IBOutlet UILabel *playerLbltime1;
@property (retain, nonatomic) IBOutlet UILabel *playerLbltime2;
@property (retain, nonatomic) IBOutlet UISlider *trackerSong;
@property (retain, nonatomic) IBOutlet UIButton *btnRepeat;
@property (retain, nonatomic) IBOutlet UIButton *btnShuffle;
@property (retain, nonatomic) IBOutlet UIButton *btnPrev;
@property (retain, nonatomic) IBOutlet UIButton *btnNext;
@property (retain, nonatomic) IBOutlet UIButton *btnPlay;

@end
