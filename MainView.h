//
//  MainView.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/20/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ShelfLabelItem.h"
#import "ShelfLabelController.h"
#import "ShelfLabelCell.h"
#import "ShelfImageView.h"
#import "CategoryListController.h"
#import "asyncimageview.h"
#import "FacebookController.h"

@interface MainView : UIViewController <UITableViewDelegate> {
    
    AppDelegate *appDelegate;

    IBOutlet UIButton *btnHilightOfWeek;
    IBOutlet UIButton *btnSong;
    IBOutlet UIButton *btnFacebook;
    
}



@end
