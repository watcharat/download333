//
//  DownloadCell.m
//  Download333
//
//  Created by Aphinop Supawaree on 12/28/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "DownloadCell.h"

@implementation DownloadCell
@synthesize imgAlbum;
@synthesize songName;
@synthesize dlProgress;
@synthesize lblDlFailed;
@synthesize btnCancel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)dealloc {
//    [imgAlbum release];
//    [songName release];
//    [dlProgress release];
//    [lblDlFailed release];
//    [btnCancel release];
//    [super dealloc];
//}
@end
