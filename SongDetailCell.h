//
//  SongDetailCell.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/27/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SongDetailCell : UITableViewCell {
    
    IBOutlet UIImageView *coverImage;
    IBOutlet UILabel *songName;
    IBOutlet UILabel *artistName;
    IBOutlet UILabel *albumName;
    IBOutlet UILabel *songLikeCount;
    IBOutlet UIButton *likeButton;
    IBOutlet UIButton *btnPreview;
    IBOutlet UIButton *btnTwiterShare;
    IBOutlet UIButton *btnFacebookShare;
    
}

@property (nonatomic, retain) UIImageView *coverImage;
@property (nonatomic, retain) UILabel *songName;
@property (nonatomic, retain) UILabel *artistName;
@property (nonatomic, retain) UILabel *albumName;
@property (nonatomic, retain) UILabel *songLikeCount;
@property (nonatomic, retain) UIButton *likeButton;
@property (nonatomic, retain) UIButton *btnPreview;
@property (nonatomic, retain) UIButton *btnTwiterShare;
@property (nonatomic, retain) UIButton *btnFacebookShare;


@end
