//
//  Library.h
//  Download333
//
//  Created by Aphinop Supawaree on 1/1/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Library : NSManagedObject

@property (nonatomic, retain) NSString * album_th;
@property (nonatomic, retain) NSString * artist_name;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * contentCode;
@property (nonatomic, retain) NSData * cover;
@property (nonatomic, retain) NSString * gmmdcode;
@property (nonatomic, retain) NSData * mvcover;
@property (nonatomic, retain) NSString * sduration;
@property (nonatomic, retain) NSString * service_id;
@property (nonatomic, retain) NSString * songname_en;
@property (nonatomic, retain) NSString * songname_th;
@property (nonatomic, retain) NSData * thmcover;
@property (nonatomic, retain) NSString * inPlaylist;

@end
