//
//  Library.m
//  Download333
//
//  Created by Aphinop Supawaree on 1/1/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Library.h"


@implementation Library

@dynamic album_th;
@dynamic artist_name;
@dynamic content;
@dynamic contentCode;
@dynamic cover;
@dynamic gmmdcode;
@dynamic mvcover;
@dynamic sduration;
@dynamic service_id;
@dynamic songname_en;
@dynamic songname_th;
@dynamic thmcover;
@dynamic inPlaylist;

@end
