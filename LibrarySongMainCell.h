//
//  LibrarySongMainCell.h
//  Download333
//
//  Created by Aphinop Supawaree on 12/31/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LibrarySongMainCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblArtistname;

@end
