//
//  GetMSISDNItems.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/13/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetMSISDNItems : NSObject {
    NSString *MSISDN;
    NSString *OPERATOR;
}

@property(nonatomic,retain) NSString *MSISDN;
@property(nonatomic,retain) NSString *OPERATOR;

@end
