//
//  LibraryMVCell.m
//  Download333
//
//  Created by Aphinop Supawaree on 1/1/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "LibraryMVCell.h"

@implementation LibraryMVCell

@synthesize btnAdd;
@synthesize imgThmCover;
@synthesize lblSongName;
@synthesize lblArtistname;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [btnAdd release];
    [imgThmCover release];
    [lblSongName release];
    [lblArtistname release];
    [super dealloc];
}

@end
