//
//  MyUtil.m
//  ArtistAppNew
//
//  Created by Aphinop Supawaree on 11/10/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "MyUtil.h"

#define TMP NSTemporaryDirectory()

@implementation MyUtil

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    
    }
    
    return self;
}

#pragma mark - Convert image to data
// method สำหรับแปลงไฟล์จาก URL เป็น NSData เพื่อเก็บลง Base
-(NSData *) convertImageToNSDataFromURL:(NSString *)url{
    
    url = [url stringByReplacingOccurrencesOfString:@" " withString:@""]; 
    url = [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSURL *image_URL = [NSURL URLWithString:url];
    NSData *image_Data = [NSData dataWithContentsOfURL:image_URL]; 
    UIImage *image = [UIImage imageWithData:image_Data];
    
    NSData *dataObj = UIImagePNGRepresentation(image);
    
    return dataObj;
}

#pragma mark - Cache image
- (void) cacheImage: (NSString *) ImageURLString andAid:(NSString *)_aid
{
    NSURL *ImageURL = [NSURL URLWithString: ImageURLString];
    
    // Generate a unique path to a resource representing the image you want
    NSString *filename = _aid;
    NSString *uniquePath = [TMP stringByAppendingPathComponent: filename];
    
    // Check for file existence
    if(![[NSFileManager defaultManager] fileExistsAtPath: uniquePath])
    {
        // The file doesn't exist, we should get a copy of it
        
        // Fetch image
        NSData *data = [[NSData alloc] initWithContentsOfURL: ImageURL];
        UIImage *image = [[UIImage alloc] initWithData: data];
        
        // Do we want to round the corners?
        image = [self roundCorners: image];
        
        // Is it PNG or JPG/JPEG?
        // Running the image representation function writes the data from the image to a file
        if([ImageURLString rangeOfString: @".png" options: NSCaseInsensitiveSearch].location != NSNotFound)
        {
            [UIImagePNGRepresentation(image) writeToFile: uniquePath atomically: YES];
        }
        else if(
                [ImageURLString rangeOfString: @".jpg" options: NSCaseInsensitiveSearch].location != NSNotFound || 
                [ImageURLString rangeOfString: @".jpeg" options: NSCaseInsensitiveSearch].location != NSNotFound
                )
        {
            [UIImageJPEGRepresentation(image, 100) writeToFile: uniquePath atomically: YES];
        }
    }
}

- (UIImage *) getCachedImage: (NSString *) ImageURLString andAid:(NSString *)_aid
{
    NSString *filename = _aid;
    NSString *uniquePath = [TMP stringByAppendingPathComponent: filename];
    
    UIImage *image;
    
    // Check for a cached version
    if([[NSFileManager defaultManager] fileExistsAtPath: uniquePath])
    {
        image = [UIImage imageWithContentsOfFile: uniquePath]; // this is the cached image
    }
    else
    {
        // get a new one
        [self cacheImage: ImageURLString];
        image = [UIImage imageWithContentsOfFile: uniquePath];
    }
    
    return image;
}

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0)
    {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

- (UIImage *) roundCorners: (UIImage*) img
{
    int w = img.size.width;
    int h = img.size.height;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
    
    CGContextBeginPath(context);
    CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
    addRoundedRectToPath(context, rect, 5, 5);
    CGContextClosePath(context);
    CGContextClip(context);
    
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), img.CGImage);
    
    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    [img release];
    
    return [UIImage imageWithCGImage:imageMasked];
}

- (void) coverPathWithImage: (NSString *) ImageURLString andAid:(NSString *)_aid{
    
    NSURL *ImageURL = [NSURL URLWithString: ImageURLString];
    NSData *data = [[NSData alloc] initWithContentsOfURL: ImageURL];
    UIImage *image = [[UIImage alloc] initWithData: data];
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
    
    
    
    
    if([ImageURLString rangeOfString: @".png" options: NSCaseInsensitiveSearch].location != NSNotFound)
    {
        NSString *filename = [[NSString alloc] initWithFormat: @"%@.png",_aid];
        NSString *uniquePath = [documentDirectory stringByAppendingPathComponent:filename];
        [UIImagePNGRepresentation(image) writeToFile: uniquePath atomically: YES];
        
    }
    else if(
            [ImageURLString rangeOfString: @".jpg" options: NSCaseInsensitiveSearch].location != NSNotFound || 
            [ImageURLString rangeOfString: @".jpeg" options: NSCaseInsensitiveSearch].location != NSNotFound
            )
    {
        NSString *filename = [[NSString alloc] initWithFormat: @"%@.jpg",_aid];
        NSString *uniquePath = [documentDirectory stringByAppendingPathComponent:filename];
        [UIImageJPEGRepresentation(image, 100) writeToFile: uniquePath atomically: YES];
        
    }
    
}

- (UIImage *)getCoverWithAid:(NSString *)_aid{
    
    
    NSString *uniquePath = [self getCoverPathWithAid:_aid];
    
    UIImage *image = [UIImage imageWithContentsOfFile:uniquePath];
    return image;
    
}

- (NSString *)getCoverPathWithAid:(NSString *)_aid{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *filename = [[NSString alloc]initWithFormat:@"%@.jpg",_aid];
    NSString *uniquePath = [documentDirectory stringByAppendingPathComponent:filename];
    
    return uniquePath;
    
}

- (int)DaysOfLastDateToNowDate:(NSDate *)_lastDate
{
//    NSDateComponents *lastDateCompoments = [[[NSDateComponents alloc] init] autorelease];
//    [lastDateCompoments setDay:22];
//    [lastDateCompoments setMonth:11];
//    [lastDateCompoments setYear:2011];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDate *lastDate = [gregorian dateFromComponents:lastDateCompoments];
    
//    NSDateFormatter *df = [NSDateFormatter new];
//    [df setDateFormat:@"dd MM YYYY"];
//    NSString *todayString = [df stringFromDate:[NSDate date]];
//    NSString *lastDateString = [df stringFromDate:_lastDate];
    
    NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:_lastDate toDate:[NSDate date] options:0];
    int numDays = [components day];
    [gregorian release];
    
    NSLog(@"num days : %d", numDays);
}


- (void)OverWriteFileInBundleName:(NSString *)_fileName withFile:(NSData *)_fileData
{
    NSFileManager *fileManager = [[[NSFileManager alloc] init] autorelease];
    //    NSLog(@"%@", [[NSBundle mainBundle] resourcePath]);
    for (NSString *fileName in [fileManager contentsOfDirectoryAtPath:[[NSBundle mainBundle] resourcePath] error:nil]) {
        
        if ([fileName rangeOfString:_fileName].location != NSNotFound) {
            
            NSLog(@"File : %@", fileName);
            
            UIImage *image = [[UIImage alloc] initWithData: _fileData];
            
//            NSString *filename = [[NSString alloc] initWithFormat: @"%@",_fileName];
            NSString *uniquePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:_fileName];
            [UIImagePNGRepresentation(image) writeToFile: uniquePath atomically: YES];
                
        
            
            
        }
        
        
    }   
    
    
}


@end
