//
//  DownloadCell.h
//  Download333
//
//  Created by Aphinop Supawaree on 12/28/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imgAlbum;
@property (retain, nonatomic) IBOutlet UILabel *songName;
@property (retain, nonatomic) IBOutlet UIProgressView *dlProgress;
@property (retain, nonatomic) IBOutlet UILabel *lblDlFailed;
@property (retain, nonatomic) IBOutlet UIButton *btnCancel;

@end
