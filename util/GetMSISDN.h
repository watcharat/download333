//
//  GetMSISDN.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/9/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "GetMSISDNItems.h"

@class AppDelegate, GetMSISDNItems;

@interface GetMSISDN : NSObject <ASIHTTPRequestDelegate,NSXMLParserDelegate> {
    AppDelegate *appDelegate;
    NSMutableString *tmpData;
    GetMSISDNItems *getMSISDNObj;}

@property(nonatomic, strong) GetMSISDNItems *getMSISDNObj;

- (void) getMSISDN;

@end
