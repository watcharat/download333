//
//  GetMSISDN.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/9/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "GetMSISDN.h"
#import "AppDelegate.h"
#import "AFXMLRequestOperation.h"

@implementation GetMSISDN
@synthesize getMSISDNObj;


NSMutableArray *getMSISDNArray;

- (id) init {
    if (self = [super init]){
		appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return self;
}


- (void) getMSISDN {
    
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/getMobileNumberFromEdgeGprs.jsp?APP_ID=%@&DEVICE=%@&APPVERSION=%@&APIVERSION=%@", APPID, appDelegate.deviceId, APPVERSION, APIVERSION];
    NSLog(@"GET MSISDN URL = %@", URL);
    NSURL *pathUrl = [NSURL URLWithString:URL];
//
//    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:pathUrl];
//    [request setDelegate:self];
//    [request startSynchronous];

    NSURLRequest *request = [NSURLRequest requestWithURL:pathUrl];
    AFXMLRequestOperation *operation = [AFXMLRequestOperation XMLParserRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSXMLParser *XMLParser) {
        getMSISDNArray = [[NSMutableArray alloc] init];
        XMLParser.delegate = self;
        [XMLParser parse];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSXMLParser *XMLParse) {
        NSLog(@"error = %@",error);
    }];
    [operation start];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    NSString *respString = [request responseString];
    NSLog(@"GET MSISDN RESPONSE DATA = %@", respString);
    NSData *responseData = [respString dataUsingEncoding:NSUTF8StringEncoding];    
    getMSISDNArray = [[NSMutableArray alloc] init];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:responseData];
    
    [parser setDelegate:self];
    [parser parse];
    [parser release];


}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"error = %@",[request error]);
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqual:@"XML"]) {
        self.getMSISDNObj = [[GetMSISDNItems alloc] init];
    }else{
        
        tmpData = [[NSMutableString alloc] init];
        
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{    [tmpData appendString:string];
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {     
    if([elementName isEqualToString:@"XML"]){
        [getMSISDNArray addObject:getMSISDNObj];
        [getMSISDNObj release];
        getMSISDNObj = nil;        
    }else if([elementName isEqualToString:@"MSISDN"]){
        [getMSISDNObj setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    }else if([elementName isEqualToString:@"OPERATOR"]){
        [getMSISDNObj setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
//    AppDelegate *appDelegate;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.MSISDN = @"";
    for (GetMSISDNItems *item in getMSISDNArray){
//        appDelegate.MSISDN = item.MSISDN;
//        MSISDN = item.MSISDN;
        appDelegate.MSISDN = [NSString stringWithString: item.MSISDN];
        appDelegate.OPERATOR = [NSString stringWithString: item.OPERATOR];
    }
}

- (void) dealloc {
    [getMSISDNObj release];
    [super dealloc];
}

@end
