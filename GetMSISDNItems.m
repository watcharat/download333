//
//  GetMSISDNItems.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/13/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "GetMSISDNItems.h"

@implementation GetMSISDNItems

@synthesize MSISDN, OPERATOR;

- (void) dealloc {
    [MSISDN release];
    [OPERATOR release];
}

@end
