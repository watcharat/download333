//
//  PlayerPlaylistCell.m
//  Download333
//
//  Created by Aphinop Supawaree on 1/2/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "PlayerPlaylistCell.h"

@implementation PlayerPlaylistCell
@synthesize imgThmcover;
@synthesize lblSongName;
@synthesize lblArtistName;
@synthesize lblAlbumName;
@synthesize playerLbltime1;
@synthesize playerLbltime2;
@synthesize trackerSong;
@synthesize btnRepeat;
@synthesize btnShuffle;
@synthesize btnPrev;
@synthesize btnNext;
@synthesize btnPlay;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [imgThmcover release];
    [lblSongName release];
    [lblArtistName release];
    [lblAlbumName release];
    [playerLbltime1 release];
    [playerLbltime2 release];
    [trackerSong release];
    [btnRepeat release];
    [btnShuffle release];
    [btnPrev release];
    [btnNext release];
    [btnPlay release];
    [super dealloc];
}
@end
