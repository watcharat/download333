//
//  PROFILE.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/13/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PROFILE : NSManagedObject{
    @private
}

@property (nonatomic, retain) NSString *msisdn;
@property (nonatomic, retain) NSDate *create_date;

@end
