//
//  SongCell.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SongCell : UITableViewCell {
    
    
    IBOutlet UIImageView *backgroundImage;
    IBOutlet UIImageView *thumbImage;
    IBOutlet UILabel *songName;
    IBOutlet UILabel *artistName;
    
}

@property (nonatomic, retain) UIImageView *backgroundImage;
@property (nonatomic, retain) UIImageView *thumbImage;
@property (nonatomic, retain) UILabel *songName;
@property (nonatomic, retain) UILabel *artistName;

@end
