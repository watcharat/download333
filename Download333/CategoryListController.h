//
//  CategoryListController.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ASIHTTPRequest.h"
#import "ShelfLabelItem.h"
#import "ShelfLabelController.h"
#import "ShelfImageView.h"
#import "SearchNavigationButton.h"
#import "SearchView.h"

@interface CategoryListController : UIViewController <NSXMLParserDelegate>{
    AppDelegate *appDelegate;
    ASIHTTPRequest *request;
    
    NSString *shelfID;
    
    ShelfLabelItem *shelfLabelItem;
    NSMutableString *tmpData;
    NSMutableArray *shelfArray;
    NSMutableArray *controller;
}

@property (nonatomic, retain) NSString *shelfID;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) ShelfLabelItem *shelfLabelItem;
@property (nonatomic, retain) NSMutableArray *controller;

- (void) connectAPI;

@end
