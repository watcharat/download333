//
//  LibrarySongMainController.m
//  Download333
//
//  Created by Aphinop Supawaree on 12/31/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "LibrarySongMainController.h"
#import "LibrarySongMainCell.h"
#import "LIbrarySongListController.h"

@implementation LibrarySongMainController
@synthesize tableData;
@synthesize artistList;
@synthesize songOfArtistList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self.navigationItem setTitle:@"Song"];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.tableData setBackgroundColor:[UIColor clearColor]];
    [self.tableData setSeparatorColor:[UIColor clearColor]];
    
}

- (void)viewDidUnload
{
    [self setTableData:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self listArtistName];
    [self.tableData reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [tableData release];
    [artistList release];
    [songOfArtistList release];
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@">>> artistList : %@", self.artistList);
    
    return [self.artistList count];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"LibrarySongMainCell";
    
    LibrarySongMainCell *Cell = (LibrarySongMainCell *)[tableData dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(Cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LibrarySongMainCell" owner:self options:nil];
        Cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    Cell.backgroundColor = [UIColor clearColor];
    Cell.lblArtistname.text = [[self.artistList objectAtIndex:indexPath.row] valueForKey:@"artist_name"];
    
    return Cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@">>> select >> artist_name : %@", [[self.artistList objectAtIndex:indexPath.row] valueForKey:@"artist_name"]);
    
    LIbrarySongListController *libSongListCtrl = [[LIbrarySongListController alloc]init];
    libSongListCtrl.searchLabel = [[self.artistList objectAtIndex:indexPath.row] valueForKey:@"artist_name"];
    [self.navigationController pushViewController:libSongListCtrl animated:YES];
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing: editing animated: animated];
    [self.tableData setEditing:editing animated:animated];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		@try {
            NSLog(@">>> Del");
            
            [self listSongWithArtistName:[[self.artistList objectAtIndex:indexPath.row]valueForKey:@"artist_name"]];
            NSLog(@"self.songOfArtistList count : %d", [self.songOfArtistList count]);
            
            for (NSArray *items in self.songOfArtistList) 
            {
                NSString *s_path = [NSString stringWithString:[self getSongFilePath:[items valueForKey:@"gmmdcode"]]];
                
                if ([[NSFileManager defaultManager] fileExistsAtPath:s_path]) {
                    [[NSFileManager defaultManager] removeItemAtPath:s_path error:nil];
                    
                }
                
            }
            
            
            [self deleteFromLibraryWithArtistName:[[self.artistList objectAtIndex:indexPath.row]valueForKey:@"artist_name"]];
            
            
            
		}
		@catch (NSException *ex) {
			NSLog(@">>> errDel : %@", ex);
		}
        
        
//        NSLog(@">>> [self.songLis count] : %d", [self.songList count]);
        [self listArtistName];
        [self.tableData reloadData];
	}
}

#pragma - mark ManagedObject
- (void)listArtistName
{
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];

	[fetchRequest setEntity:entity];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"artist_name"]]];
    [fetchRequest setReturnsDistinctResults:YES];
    [fetchRequest setResultType:NSDictionaryResultType];
	
    NSString *predicateCondition = [NSString stringWithFormat:@"service_id == '1'"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
	
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"artist_name" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    
    NSError *error = nil;
    NSArray *distincResults = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];

	
	[fetchRequest release];
	
	if ([distincResults count]) {
        
       self.artistList = [NSArray arrayWithArray:distincResults];
		
	}
    else
    {
        self.artistList = nil;
    }
	
}

- (void)listSongWithArtistName:(NSString *)_artist_name
{
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    
    [fetchRequest setResultType:NSDictionaryResultType];
    
	[fetchRequest setEntity:entity];
	
    NSString *predicateCondition = [NSString stringWithFormat:@"artist_name = '%@' AND service_id = 1", _artist_name];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
	
    
    NSError *error = nil;
    NSArray *results = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
	
	[fetchRequest release];
    
	if ([results count]) {
        
        self.songOfArtistList = [NSArray arrayWithArray:results];
	}
    else
    {
        self.songOfArtistList = nil;
    }
	
}

- (void)deleteFromLibraryWithArtistName:(NSString *)_artist_name
{
    
    NSLog(@">>> _artist_name : %@",_artist_name);
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSString *predicateCondition = [NSString stringWithFormat:@"artist_name = '%@' AND service_id = 1",_artist_name];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *items = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    
    
    for (NSManagedObject *managedObject in items) {
        [appDelegate.managedObjectContext deleteObject:managedObject];
        //        NSLog(@"%@ object deleted",entityname);
    }
    if (![appDelegate.managedObjectContext save:&error]) {
        //        NSLog(@"Error deleting %@ - error:%@",entityname,error);
    }
}

#pragma mark - song path
- (NSString *)getSongFilePath:(NSString *)nameOfFile
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *filename = [[NSString alloc]initWithFormat:@"%@.mp3",nameOfFile];
    NSString *uniquePath = [documentDirectory stringByAppendingPathComponent:filename];
    
    return uniquePath;
    
}

@end
