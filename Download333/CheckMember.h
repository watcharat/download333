//
//  CheckMember.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/22/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SubScribtMember.h"

@interface CheckMember : UIViewController<NSXMLParserDelegate> {
    AppDelegate *appDelegate;
    ASIHTTPRequest *request;
    NSString *tmpData;
    NSString *STATUS;
    NSString *STATUS_CODE;
    NSString *STATUS_DETAIL;
    NSString *ACCOUNT_ID;
    NSString *ACCOUNT_NAME;
    NSString *DATE_EXPIRE;
}

@property (retain, nonatomic) NSString *STATUS;
@property (retain, nonatomic) NSString *STATUS_CODE;
@property (retain, nonatomic) NSString *STATUS_DETAIL;
@property (retain, nonatomic) NSString *ACCOUNT_ID;
@property (retain, nonatomic) NSString *ACCOUNT_NAME;
@property (retain, nonatomic) NSString *DATE_EXPIRE;

- (void) alertMessageWithTitle:(NSString*) title andMessage:(NSString*) message;
- (void) connectAPI;

@end
