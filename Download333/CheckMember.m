//
//  CheckMember.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/22/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "CheckMember.h"
#import "AFXMLRequestOperation.h"
@implementation CheckMember

@synthesize STATUS, STATUS_CODE, STATUS_DETAIL, ACCOUNT_ID, ACCOUNT_NAME, DATE_EXPIRE;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setHidden:true];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self connectAPI];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [request release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Connect API
- (void) connectAPI {
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/getAccountDetailFromMSISDN.jsp?APP_ID=%@&APIVERSION=%@&MSISDN=%@&DEVICE=%@&APPVERSION=%@", APPID, APIVERSION, appDelegate.MSISDN, appDelegate.deviceId, APPVERSION];
    
    NSURL *pathUrl = [NSURL URLWithString:URL];
//    NSLog(@"url = %@", pathUrl);
//    
//    request = [ASIHTTPRequest requestWithURL:pathUrl];
//    [request setDelegate:self];
//    [request startSynchronous];

    NSURLRequest *request = [NSURLRequest requestWithURL:pathUrl];
    AFXMLRequestOperation *operation = [AFXMLRequestOperation XMLParserRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSXMLParser *XMLParser) {
        XMLParser.delegate = self;
        [XMLParser parse];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSXMLParser *XMLParse) {
        NSLog(@"error = %@",error);
    }];
    [operation start];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    NSString *respString = [request responseString];
    NSData *responseData = [respString dataUsingEncoding:NSUTF8StringEncoding];    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:responseData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"error = %@",[request error]);
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {    
    tmpData = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {     
    
    if([elementName isEqualToString:@"STATUS"]) {
        self.STATUS = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"STATUS_CODE"]) {
        self.STATUS_CODE = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"STATUS_DETAIL"]) {
        self.STATUS_DETAIL = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"ACCOUNT_ID"]) {
        self.ACCOUNT_ID = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"ACCOUNT_NAME"]) {
        self.ACCOUNT_NAME = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"DATE_EXPIRE"]) {
        self.DATE_EXPIRE = [NSString stringWithFormat:@"%@", tmpData];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"CHECK MEMBER STATUS = %@", STATUS);
    NSLog(@"CHECK MEMBER STATUS_CODE = %@", STATUS_CODE);
    NSLog(@"CHECK MEMBER STATUS_DETAIL = %@", STATUS_DETAIL);
    NSLog(@"CHECK MEMBER ACCOUNT_ID = %@", ACCOUNT_ID);
    NSLog(@"CHECK MEMBER ACCOUNT_NAME = %@", ACCOUNT_NAME);
    NSLog(@"CHECK MEMBER DATE_EXPIRE = %@", DATE_EXPIRE);
    
    //maz
    if([STATUS isEqualToString:@"OK"] && [STATUS_CODE isEqualToString:@"200"]){
        // TODO :: MainView
        [self.view removeFromSuperview];
        [appDelegate saveProfileWithMSISDN:appDelegate.MSISDN andCreateDate:[NSDate date]];
//        [self.view setHidden:true];
        //[appDelegate.window addSubview:checkMember.view];
        appDelegate.window.rootViewController = appDelegate.mainTab;
        
    } else if([STATUS isEqualToString:@"ERR"] && [STATUS_CODE isEqualToString:@"107"]) {
    
//        NSEntityDescription *entity = [NSEntityDescription entityForName:@"PROFILE" inManagedObjectContext:appDelegate.managedObjectContext];
//        
//        NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
//        [request setEntity:entity];
//        NSArray *profiles = [appDelegate.managedObjectContext executeFetchRequest:request error:nil];
//        
//        for(PROFILE *profile in profiles) {
//            [appDelegate.managedObjectContext deleteObject:profile];
//        }
//    
//        NSError* er;
//        [appDelegate.managedObjectContext save:&er];
    
        NSLog(@"RETURN 107, NOT MEMBER");
        [self.view setHidden:false];
    } else {
        [self alertMessageWithTitle:STATUS andMessage:STATUS_DETAIL];
    }
    
}


#pragma mark - Button Submit Control
- (IBAction)btnSubmitTouchUpInside:(id)sender {
    [self.view removeFromSuperview];
    SubScribtMember *subMember = [[SubScribtMember alloc] init];
    [appDelegate.window addSubview:subMember.view];
}


#pragma mark - Buton Close Control
- (IBAction)btnCloseTouchUpInside:(id)sender {
    [self.view removeFromSuperview];
    [appDelegate exitView];
}

#pragma mark - Alert Message
- (void) alertMessageWithTitle:(NSString*) title andMessage:(NSString*) message {
    UIAlertView *errView;
    
    errView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@", title] 
                                         message:[NSString stringWithFormat:@"%@", message] 
                                        delegate:self 
                               cancelButtonTitle:@"OK" 
                               otherButtonTitles:nil];
    [errView show];
    [errView release];
}


@end





