//
//  ValidateOTP.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/19/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "ValidateOTP.h"
#import "AFXMLRequestOperation.h"
@implementation ValidateOTP
@synthesize txtValidateCode;

@synthesize STATUS, STATUS_CODE, STATUS_DETAIL;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    NSLog(@"AppDelegate MSISDN = %@", appDelegate.MSISDN);
}

- (void)viewDidUnload
{
    [self setTxtValidateCode:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Input Text Validate Code Control
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(txtValidateCode.text.length >=4 && range.length == 0)
        return NO;
    else
        return YES;
}


#pragma mark - Submit Button Control
- (IBAction)btnSubmitTouchUpInside:(id)sender {
    if(txtValidateCode.text.length != 4){
        [self alertMessageWithTitle:@"ข้อมูลไม่ครบถ้วน" 
                         andMessage:@"รหัสผ่านที่ท่านกรอกไม่ครบ 4 หลัก กรุณาตรวจสอบรหัสผ่านค่ะ"];
    }else{
        [self connectAPIWithMSISDN:appDelegate.MSISDN andValidateCode:txtValidateCode.text];
    }
}


#pragma mark - Alert Message
- (void) alertMessageWithTitle:(NSString*) title andMessage:(NSString*) message {
    UIAlertView *errView;
    
    errView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@", title] 
                                         message:[NSString stringWithFormat:@"%@", message] 
                                        delegate:self 
                               cancelButtonTitle:@"OK" 
                               otherButtonTitles:nil];
    [errView show];
    [errView release];
}


#pragma mark - Connect API
- (void) connectAPIWithMSISDN:(NSString *) msisdnRQ andValidateCode:(NSString *) validateCodeRQ {
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/validateOTP.jsp?APP_ID=%@&APIVERSION=%@&MSISDN=%@&ONETIMEPASS=%@&DEVICE=%@&APPVERSION=%@", APPID, APIVERSION, msisdnRQ, validateCodeRQ, appDelegate.deviceId, APPVERSION];
    NSLog(@"URL BEFORE CALL %@",URL);
    NSURL *pathUrl = [NSURL URLWithString:URL];
    NSLog(@"url = %@", pathUrl);
//    
//    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:pathUrl];
//    [request setDelegate:self];
//    [request startSynchronous];
//    [request release];
    NSURLRequest *request = [NSURLRequest requestWithURL:pathUrl];
    AFXMLRequestOperation *operation = [AFXMLRequestOperation XMLParserRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSXMLParser *XMLParser) {
        XMLParser.delegate = self;
        [XMLParser parse];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSXMLParser *XMLParse) {
        NSLog(@"error = %@",error);
    }];
    [operation start];
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    NSString *respString = [request responseString];
    NSData *responseData = [respString dataUsingEncoding:NSUTF8StringEncoding];    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:responseData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"error = %@",[request error]);
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{

}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {    
    tmpData = string;
    NSLog(@"validate OPT :%@",tmpData);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {     
    
    if([elementName isEqualToString:@"STATUS"]) {
        self.STATUS = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"STATUS_CODE"]) {
        self.STATUS_CODE = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"STATUS_DETAIL"]) {
        self.STATUS_DETAIL = [NSString stringWithFormat:@"%@", tmpData];
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"End otpvalidate");
    NSLog(@"STATUS = %@", self.STATUS);
    NSLog(@"STATUS_CODE = %@", self.STATUS_CODE);
    NSLog(@"STATUS_DETAIL = %@", self.STATUS_DETAIL);
    
    if([STATUS isEqualToString:@"OK"] && [STATUS_CODE isEqualToString:@"200"]){
        [self.view removeFromSuperview];
        CheckMember *checkMember = [[CheckMember alloc] init];
        [appDelegate.window addSubview:checkMember.view];
//        SubScribtMember *subscribtMember = [[SubScribtMember alloc] init];
//        [appDelegate.window addSubview:subscribtMember.view];
    }else{
        [self alertMessageWithTitle:STATUS andMessage:STATUS_DETAIL];
    }

}

#pragma mark - Keyboard Control
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [txtValidateCode resignFirstResponder];
    return YES;
}


#pragma mark - Close Button Control
- (IBAction)btnCloseTouchUpInside:(id)sender {
    [self.view removeFromSuperview];
    [appDelegate exitView];
}


#pragma mark - Dealloc
- (void)dealloc {
    [txtValidateCode release];
    [super dealloc];
}
@end
