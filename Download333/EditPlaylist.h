//
//  EditPlaylist.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/20/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "PlayerPlaylistSongCell.h"
#import "Library.h"
#import "SongPlayer.h"

@interface EditPlaylist : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
    AppDelegate *appDelegate;
    IBOutlet UITableView *tableView;
    NSArray *songList;
    BOOL firstLoad;
    
}

@property (nonatomic, retain) UITableView *tableView;
@property (retain, nonatomic) NSArray *songList;

- (void) deleteFromLibrary;
- (void)listSong;

@end
