//
//  SearchView.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/5/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "SearchView.h"

@implementation SearchView


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    self.navigationItem.title = @"Search";
    
    [searchBar setPlaceholder:@"Music Search"];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // ---- Setting Navigate Left Button
    BackNavigateButton *btnBackView = [[BackNavigateButton alloc] init];
    [btnBackView addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithCustomView:btnBackView];
    self.navigationItem.leftBarButtonItem = btnBack;
    // ---- End Setting Navigate Left Button
        
}

- (void)Back:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES]; 
    if([[self.navigationController childViewControllers] count] >0 ){
        [self.navigationController popViewControllerAnimated:YES]; 
    }
}

- (void)viewDidUnload
{
    [searchBar release];
    searchBar = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [searchBar becomeFirstResponder];
    for (UIView *searchBarSubview in [searchBar subviews]) {
        
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            
            @try {
                
                [(UITextField *)searchBarSubview setReturnKeyType:UIReturnKeyDone];
                [(UITextField *)searchBarSubview setKeyboardAppearance:UIKeyboardAppearanceAlert];
            }
            @catch (NSException * e) {
                
                // ignore exception
            }
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [searchBar release];
    [super dealloc];
}


-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"TODO SEARCH");
    
    NSString *searchText = searchBar.text;
        
    [appDelegate.contentList removeAllObjects];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    ShelfLabelController *nextController = [[ShelfLabelController alloc] initWithNibName:@"SongListView" bundle:[NSBundle mainBundle]];
    
    nextController.NAMETH = @"Search";
    nextController.NAMEEN = @"Search Result";
    nextController.SHELF_ID = @"";
    nextController.searchString = searchText;
    
    [appDelegate.mainViewController pushViewController:nextController animated:YES];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}



@end








