//
//  MVPlayer.m
//  Download333
//
//  Created by Aphinop Supawaree on 1/3/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "MVPlayer.h"
#import "AppDelegate.h"

#define degreesToRadian(x) (M_PI * (x) / 180.0)

@interface MyMovieViewController : MPMoviePlayerViewController

@end

@implementation MyMovieViewController
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

@end


@implementation MVPlayer
@synthesize gmmdcode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	[self setWantsFullScreenLayout:YES];
    //    [[UIApplication sharedApplication] setStatusBarHidden:YES animated:NO];
    
    
    self.view.frame = [[UIScreen mainScreen] bounds];
    nowPlaying = NO;
    
	NSString *path = [self getSongFilePath:self.gmmdcode];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if([fileManager fileExistsAtPath:path]) {
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDidLoad:) name:@"MPMoviePlayerContentPreloadDidFinishNotification" object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDidFinish:) name:@"MPMoviePlayerPlaybackDidFinishNotification" object:nil];
		
		
		if (NSClassFromString(@"MPMoviePlayerViewController") != nil
			&& [UIViewController instancesRespondToSelector: @selector(presentMoviePlayerViewControllerAnimated:)]) {
			
			
            
            NSString *osversion = [[UIDevice currentDevice] systemVersion];
            NSLog(@"osversion = %@",osversion);
            if ([[osversion substringWithRange:NSMakeRange(0,1)] caseInsensitiveCompare:@"7"] == NSOrderedSame) {
                
                iosSeven = YES;
                
                [self.view removeFromSuperview];
                self.player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
                self.player.view.frame = self.view.frame;
                
                self.player.controlStyle = MPMovieControlStyleFullscreen;//MPMovieControlStyleEmbedded;
                [self.player setFullscreen:YES];
                [self.view addSubview:self.player.view];
                [self.player play];
                
            }else{
                
                // iOS 4
                iosSeven = NO;
                
                CGSize containerSize  = self.view.frame.size;   // Container NOT rotated!
                float  videoWidth     = 384;                     // Keep 16x9 aspect ratio
                float  videoHeight    = 216;
                
                [self.view removeFromSuperview];
                
                
                previewMP = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
                previewMP.view.backgroundColor = [UIColor blackColor];
                 
                previewMP.view.transform = CGAffineTransformMakeRotation(degreesToRadian(90));
                
                // This video player is rotated so flip width and height, but the container view
                // isn't rotated!
                [previewMP.view setFrame:CGRectMake((containerSize.width-videoHeight)/2,
                                                        (containerSize.height-videoWidth)/2,
                                                        videoHeight,
                                                        videoWidth)];
                [self.view addSubview:previewMP.view];
                [previewMP moviePlayer].fullscreen = TRUE;
                [[previewMP moviePlayer] play];

            }
            
            
		}
		else {
            
            
            NSString *osversion = [[UIDevice currentDevice] systemVersion];
            NSLog(@"osversion = %@",osversion);
            if ([[osversion substringWithRange:NSMakeRange(0,1)] caseInsensitiveCompare:@"7"] == NSOrderedSame) {
                
                iosSeven = YES;
                
                [self.view removeFromSuperview];
                //                [appDelegate.window.rootViewController.view addSubview:previewMP.view];
                
                self.player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
                self.player.view.frame = self.view.bounds;
                
                self.player.controlStyle = MPMovieControlStyleFullscreen;//MovieControlStyleEmbedded;
                [self.player setFullscreen:YES];
                [self.view addSubview:self.player.view];
                
                [self.player play];
                
            }else{
                
                iosSeven = NO;
                
                //			previewMP = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
                //			previewMP.movieControlMode = MPMovieControlModeDefault;
                //			previewMP.scalingMode = MPMovieScalingModeAspectFill;
                //			previewMP.fullscreen = YES;
                
                NSURL *urlFilePath = [NSURL fileURLWithPath:path];
                previewMP = [[MPMoviePlayerViewController alloc] initWithContentURL:urlFilePath];
                previewMP.moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
                //            previewMP.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
                //            previewMP.moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
                [previewMP.moviePlayer play];
            }
            
            
            
		}
        
        if(self.player){
            if (self.interfaceOrientation == UIInterfaceOrientationPortrait) {
                
                self.player.view.transform = CGAffineTransformIdentity;
                self.player.view.transform = CGAffineTransformMakeRotation(degreesToRadian(90));
                self.player.view.frame = [[UIScreen mainScreen] bounds];
                
                //        self.view.transform = CGAffineTransformIdentity;
                //        self.view.transform = CGAffineTransformMakeRotation(degreesToRadian(90));
            }
            
        }
        
		
	}
	fileManager = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = ni
}



- (void)viewWillAppear:(BOOL)animated {
    
    [[[SongPlayer sharedInstance] audioPlayer] pause];
    [[SongPlayer sharedInstance] setPlayState:NO];
    [[SongPlayer sharedInstance] changeBtnPlayer];
    
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait) {
        
        self.player.view.transform = CGAffineTransformIdentity;
        self.player.view.transform = CGAffineTransformMakeRotation(degreesToRadian(90));
        self.player.view.frame = [[UIScreen mainScreen] bounds];
        
        //        self.view.transform = CGAffineTransformIdentity;
        //        self.view.transform = CGAffineTransformMakeRotation(degreesToRadian(90));
	}
	
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    portrait = UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    
    // Code to update subview layout goes here
}

-(void)viewWillLayoutSubviews
{
    BOOL isPortraitNow = UIInterfaceOrientationIsPortrait(self.interfaceOrientation);
    
    if(isPortraitNow != portrait)
    {
        NSLog(@"Interfaceorientation mismatch!, correcting");
        
        portrait = isPortraitNow;
        
        // Code to update subview layout goes here
    }
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//    if(iosSeven && nowPlaying){
//        return YES;
//    }else{
//        return NO;
//    }
//
//}


- (void)movieDidLoad:(id)sender {
	/*NSLog(@"movie Loading...");
	 [myActivity stopAnimating];
	 [appDelegate stopWaiting];
	 [myMP play];*/
}

- (void)movieDidFinish:(id)sender
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"MPMoviePlayerContentPreloadDidFinishNotification" object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"MPMoviePlayerPlaybackDidFinishNotification" object:nil];
    
    [self.view removeFromSuperview];
    
    if(iosSeven){
        [self.player release];
        self.player = nil;
    }else{
        [previewMP release];
        previewMP = nil;
    }
	
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    //    [[UIApplication sharedApplication] setStatusBarHidden:NO animated:NO];
	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}

#pragma mark - song path
- (NSString *)getSongFilePath:(NSString *)nameOfFile
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *filename = [[NSString alloc]initWithFormat:@"%@.mp4",nameOfFile];
    NSString *uniquePath = [documentDirectory stringByAppendingPathComponent:filename];
    
    return uniquePath;
    
}
- (BOOL)shouldAutorotate
{
    return YES;
}



@end
