//
//  SearchView.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/5/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ShelfLabelController.h"

@interface SearchView : UIViewController <UISearchBarDelegate> {
    
    AppDelegate *appDelegate;
    IBOutlet UISearchBar *searchBar;
    
}

@end
