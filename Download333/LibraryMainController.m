//
//  LibraryMainController.m
//  Download333
//
//  Created by Aphinop Supawaree on 12/31/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "LibraryMainController.h"
#import "LibrarySongMainController.h"
#import "LibraryMVListController.h"

@implementation LibraryMainController
@synthesize btnLibrarySong;
@synthesize btnLibraryMV;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor clearColor]];
}

- (void)viewDidUnload
{
    [self setBtnLibrarySong:nil];
    [self setBtnLibraryMV:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [btnLibrarySong release];
    [btnLibraryMV release];
    [super dealloc];
}
- (IBAction)pressToSong:(id)sender {
    
    [sender setImage:[UIImage imageNamed:@"bt_LB_song_on.png"] forState:UIControlStateNormal];
    
    [self.btnLibraryMV setImage:[UIImage imageNamed:@"bt_LB_mv_off.png"] forState:UIControlStateNormal];
    
    LibrarySongMainController *libSongMainCtrl = [[LibrarySongMainController alloc]init];
    [self.navigationController pushViewController:libSongMainCtrl animated:YES];
    [libSongMainCtrl release];
    
}

- (IBAction)pressToMV:(id)sender {
    
    [sender setImage:[UIImage imageNamed:@"bt_LB_mv_on.png"] forState:UIControlStateNormal];
    
    [self.btnLibrarySong setImage:[UIImage imageNamed:@"bt_LB_song_off.png"] forState:UIControlStateNormal];
    
    LibraryMVListController *libMVCtrl = [[LibraryMVListController alloc]init];
    [self.navigationController pushViewController:libMVCtrl animated:YES];
    [libMVCtrl release];
}
@end
