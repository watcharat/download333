//
//  PlaylistMainController.m
//  Download333
//
//  Created by Aphinop Supawaree on 1/1/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "PlaylistMainController.h"
#import "PlaylistSongCell.h"
#import "PlaylistClearAllCell.h"
#import "Library.h"
#import "SongPlayer.h"

@implementation PlaylistMainController
@synthesize tableData;
@synthesize songList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self.navigationItem setTitle:@"Song"];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.tableData setBackgroundColor:[UIColor clearColor]];
    [self.tableData setSeparatorColor:[UIColor clearColor]];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [self listSong];
//    [self.tableData reloadData];
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1; 
  
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    NSLog(@">>> songList : %@", self.songList);
    return [self.songList count];
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 78;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *cellIdentifier = @"PlaylistSongCellIdentifier";
    
    PlaylistSongCell *Cell = (PlaylistSongCell *)[tableData dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(Cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlaylistSongCell" owner:self options:nil];
        Cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    //    [menuButton addTarget:self action:@selector(mainMenu_Clicked_back:) forControlEvents:UIControlEventTouchUpInside];
    
    Cell.backgroundColor = [UIColor clearColor];
    Cell.imgThmCover.image = [UIImage imageWithData:[[self.songList objectAtIndex:indexPath.row]valueForKey:@"thmcover"]];
    Cell.lblSongName.text = [[self.songList objectAtIndex:indexPath.row]valueForKey:@"songname_th"];
    Cell.lblArtistname.text = [[self.songList objectAtIndex:indexPath.row]valueForKey:@"artist_name"];
    return Cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SongPlayer *songPlayer = [SongPlayer sharedInstance];
    
    if (![songPlayer.songIndex isEqualToString:[NSString stringWithFormat:@"%d",indexPath.row]]) {
        songPlayer.songIndex = [NSString stringWithFormat:@"%d", indexPath.row];
       [songPlayer PlayWhenYesPlayState]; 
    }
    [self.navigationController pushViewController:songPlayer animated:YES];
    
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing: editing animated: animated];
    [self.tableData setEditing:editing animated:animated];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		@try {
            NSLog(@">>> Del");
            
            [self removeSongFromPlaylistWithGMMDCode:[[self.songList objectAtIndex:indexPath.row]valueForKey:@"gmmdcode"]];
            
            
		}
		@catch (NSException *ex) {
			NSLog(@">>> errDel : %@", ex);
		}
        
        [self listSong];
        
        NSLog(@">>> [self.songList count] : %d", [self.songList count]);
        
        [self.tableData reloadData];
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Remove";
}

#pragma - mark ManagedObject
- (void)listSong
{
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    
    [fetchRequest setResultType:NSDictionaryResultType];
    
	[fetchRequest setEntity:entity];
	
    NSString *predicateCondition = [NSString stringWithFormat:@"service_id = 1 AND inPlaylist = 'YES'"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
	
    
    NSError *error = nil;
    NSArray *results = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
	
	[fetchRequest release];
	
	if ([results count]) {
        
        self.songList = [NSArray arrayWithArray:results];
	}
    else
    {
        self.songList = nil;
    }
	
    appDelegate.playlistSongList = self.songList;
}

- (void)removeSongFromPlaylistWithGMMDCode:(NSString *)_gmmdcode
{
    
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" 
											  inManagedObjectContext:appDelegate.managedObjectContext];
	[fetchRequest setEntity:entity];
	
    NSString *predicateCondition = [NSString stringWithFormat:@"gmmdcode == %@ AND service_id = 1 AND inPlaylist = 'YES'", _gmmdcode];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
	
	NSError *error;
	NSArray *items = [appDelegate.managedObjectContext
					  executeFetchRequest:fetchRequest error:&error];
	
	[fetchRequest release];
	
	if ([items count]) {
        
        for (Library *libItems in items) {
            libItems.inPlaylist = @"NO";
        }
        
	}
    
    [appDelegate.managedObjectContext save:nil];
    
    [self listSong];
	[self.tableData reloadData];
    
}


@end
