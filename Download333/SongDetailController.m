//
//  SongDetailController.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "SongDetailController.h"
#import "XMLDownload.h"

@implementation SongDetailController

@synthesize Content;
@synthesize ContentCode;
@synthesize GMMDCode;
@synthesize sVide;
@synthesize SongNameEN;
@synthesize SongNameTH;
@synthesize thmCover;
@synthesize cover;
@synthesize AlbumTH;
@synthesize ArtistEN;
@synthesize ArtistTH;
@synthesize preview;
@synthesize sDuration;
@synthesize rbt;
@synthesize CP_ID;
@synthesize mvCover;
@synthesize streaming_mv;
@synthesize streaming_fs;
@synthesize lyrics;

#define TIME_PREVIEW 15


@synthesize tableView;

@synthesize player;

@synthesize timer;

@synthesize facebook;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    self.tableView.backgroundColor = [UIColor clearColor];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    playing = NO;
    boolLikeStatus = NO;
    
    self.navigationItem.title = SongNameTH;
    
    [self getLikeInfo];
}

//- (void)loadView {
//    [super loadView];
//    if ([TWTweetComposeViewController class] != nil) {
//        if([TWTweetComposeViewController canSendTweet]) {
//            _canTweet = YES;
//        }
//    }
//}

- (void)viewDidUnload
{
    [tableView release];
    tableView = nil;
//    [player release];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewDidDisappear:(BOOL)animated {
    [player pause];
//    [player release];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [Content release];
    [ContentCode release];
    [GMMDCode release];
    [sVide release];
    [SongNameEN release];
    [SongNameTH release];
    [thmCover release];
    [cover release];
    [AlbumTH release];
    [ArtistEN release];
    [ArtistTH release];
    [preview release];
    [sDuration release];
    [rbt release];
    [CP_ID release];
    [mvCover release];
    [streaming_mv release];
    [streaming_fs release];
    [lyrics release];
    [tableView release];
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) {
        return 1;
    } else {
        int countContentList = [appDelegate.contentList count];
        NSLog(@"count = %D", countContentList);
        if(countContentList > 5){
            return 5;
        }else{
            return countContentList;
        }
//        return 5;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//	if (section == 1 && memberFlag == YES) return @"Top 5";
//	else return nil;
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        return 200;
    } else {
        return 78;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        static NSString *SONG_DETAIL_CELL_IDENTIFIER = @"SongDetailCellIdentifier";
        SongDetailCell *songDetailCell = (SongDetailCell *)[tableView dequeueReusableCellWithIdentifier:SONG_DETAIL_CELL_IDENTIFIER];
        
        if(songDetailCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SongDetailCell" owner:self options:nil];
            songDetailCell = [nib objectAtIndex:0];
        }
        
        CGRect frame;
        frame.size.width = 90;
        frame.size.height = 90;
        frame.origin.x = 0;
        frame.origin.y = 0;
        [[songDetailCell.contentView viewWithTag:1] removeFromSuperview];
        ShelfImageView *myImage = [[ShelfImageView alloc] initWithFrame:frame];
        myImage.tag = 1;
        [NSThread detachNewThreadSelector:@selector(thumbnailImage:) toTarget:myImage withObject:[NSString stringWithFormat:@"%@", thmCover]];
        
        [songDetailCell.coverImage addSubview:myImage];
        songDetailCell.songName.text = SongNameTH;
        songDetailCell.artistName.text = ArtistTH;
        songDetailCell.albumName.text = AlbumTH;
        
        songDetailCell.backgroundColor = [UIColor clearColor];
        if([likeStatusCode isEqualToString:@"501"]){
            UIImage *img = [UIImage imageNamed:@"bt_like.png"];
            [songDetailCell.likeButton setImage:img forState:UIControlStateNormal];
            [img release];
        }else if([likeStatusCode isEqualToString:@"502"]){
            UIImage *img = [UIImage imageNamed:@"bt_Unlike.png"];
            [songDetailCell.likeButton setImage:img forState:UIControlStateNormal];
            [img release];
        }else if([likeStatusCode isEqualToString:@"503"]){
            UIImage *img = [UIImage imageNamed:@"bt_like.png"];
            [songDetailCell.likeButton setImage:img forState:UIControlStateNormal];
            [img release];
        }else{
            UIImage *img = [UIImage imageNamed:@"bt_like.png"];
            [songDetailCell.likeButton setImage:img forState:UIControlStateNormal];
            [img release];
        }
        
        if (playing){
            UIImage *img = [UIImage imageNamed:@"bt_pause.png"];
            [songDetailCell.btnPreview setImage:img forState:UIControlStateNormal];
            [img release];
        }
        
        songDetailCell.songLikeCount.text = likeCount;
               
        [myImage release];
        
        return songDetailCell;
    } else {
        static NSString *SONG_DETAIL_CELL_IDENTIFIER = @"SongDetailRelateCellIdentifier";
        SongDetailRelateCell *cell = (SongDetailRelateCell *)[tableView dequeueReusableCellWithIdentifier:SONG_DETAIL_CELL_IDENTIFIER];
        
        if(cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SongDetailRelateCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        SongItem *content = [appDelegate.contentList objectAtIndex:indexPath.row];
//        NSLog(@"%@", content);
        
        cell.songName.text = content.SongNameTH;
        cell.artistName.text = content.ArtistTH;
        cell.backgroundColor = [UIColor clearColor];
        CGRect frame;
        
        frame.size.width = 50;
        frame.size.height = 50;
        frame.origin.x = 0;
        frame.origin.y = 0;
        
        [[cell.contentView viewWithTag:1] removeFromSuperview];
        
        ShelfImageView *myImage = [[ShelfImageView alloc] initWithFrame:frame];
        myImage.tag = 1;
        
        [NSThread detachNewThreadSelector:@selector(thumbnailImage:) toTarget:myImage withObject:[NSString stringWithFormat:@"%@", content.thmCover]];
        
        [cell.thumbImage addSubview:myImage];
        
        [myImage release];
        
        return cell;
    }
    
}



- (IBAction)playPreview:(id)sender {
    NSLog(@"TODO :: play preview, path = %@", preview);
    if (playing){
        [player pause];
        playing = NO;
        UIImage *img = [UIImage imageNamed:@"bt_preview_default.png"];
        [sender setImage:img forState:UIControlStateNormal];
        [img release];
//        [timer invalidate];
    } else {
        if(![preview isEqualToString:@""]){
            
            UIImage *img = [UIImage imageNamed:@"bt_pause.png"];
            [sender setImage:img forState:UIControlStateNormal];
            [img release];
            
            //btnPlay = (UIButton*) sender;
            if(sender){
                if ([sender isKindOfClass:[UIButton class]])
                {
                    NSLog(@"UIButton");
                    btnPlay = sender;
                }else{
                    NSLog(@"NOT UIButtton");
                    btnPlay = NULL;
                }   
            }else{
                NSLog(@"sender = nil");
                btnPlay = NULL;
            }
             
                 
            

//            timer = [NSTimer scheduledTimerWithTimeInterval:TIME_PREVIEW target:self selector:@selector(completePreviewTime) userInfo:NULL repeats:NO];
            
            
            player = [[AVPlayer playerWithURL:[NSURL URLWithString:preview]] retain];
            [player play];
            playing = YES;
            
            
//            UIImage *img = [UIImage imageNamed:@"bt_pause.png"];
//            [sender setImage:img forState:UIControlStateNormal];
//            [img release];
        }
    }
}

-(void)completePreviewTime{
    playing = NO;
//    [timer invalidate];
    
    
    @try {
        if(btnPlay){
            [btnPlay setImage:[UIImage imageNamed:@"bt_preview_default.png"] forState:UIControlStateNormal];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"error : %@", exception.description);
    }
    
}


#pragma mark - Download and Action sheet

- (IBAction)downloadActionSheet:(id)sender
{
    if ([sVide isEqualToString:@"yes"]) {
        UIActionSheet *downloadAS = [[UIActionSheet alloc] initWithTitle:@"Download" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Song", @"MV", nil];
        downloadAS.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [downloadAS showInView:self.view];
        [downloadAS release];
    }
    
    if ([sVide isEqualToString:@""]) {
        UIActionSheet *downloadAS = [[UIActionSheet alloc] initWithTitle:@"Download" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Song", nil];
        downloadAS.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [downloadAS showInView:self.view];
        [downloadAS release];
    }
   
//    if ([streaming_fs isEqualToString:@""] && [streaming_mv isEqualToString:@""] ) {
//        UIActionSheet *downloadAS = [[UIActionSheet alloc] initWithTitle:@"Download" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Song", @"MV", nil];
//        downloadAS.actionSheetStyle = UIActionSheetStyleBlackOpaque;
//        [downloadAS showInView:self.view];
//        [downloadAS release];
//    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([sVide isEqualToString:@"yes"]) {
        if (buttonIndex == 0) {		
            NSLog(@">>> Song");
            [self postDownloadURLInBackgroundWithType:@"1"];
        } else if (buttonIndex == 1) {
            NSLog(@">>> MV");
            [self postDownloadURLInBackgroundWithType:@"2"];
        } else if (buttonIndex == 2) {
            NSLog(@">>> Cancel");
        }
    }
    
    if ([sVide isEqualToString:@""] ) {
        if (buttonIndex == 0) {		
            NSLog(@">>> Song | GMMDCode : %@", self.GMMDCode);
            [self postDownloadURLInBackgroundWithType:@"1"];
        } else if (buttonIndex == 1) {
            NSLog(@">>> Cancel");
        } 
    }
    
//    
//    if ([streaming_fs isEqualToString:@""] && [streaming_mv isEqualToString:@""] ) {
//        if (buttonIndex == 0 || buttonIndex == 1) {		
//            NSLog(@">>> index : 0");
//            [self totabDownload];
//        } else if (buttonIndex == 2) {
//            NSLog(@">>> index : 2");
//        }
//    }
    
}

- (void)totabDownload
{
    appDelegate.mainTab.selectedIndex = 3;
//    NSLog(@"appDelegate.mainTab.viewControllers : %@", appDelegate.mainTab.viewControllers);
}

- (void)addSongToDownloadWithURL:(NSString *)_url andService:(NSString *)_SERVICE_ID
{
    
    BOOL flag = NO;
    if ([[DownloadController sharedInstance].downloadList count] > 0) {
        
        for (NSArray *items in [DownloadController sharedInstance].downloadList) {
            
//            NSLog(@">>> items : %@", ((DownloadItem*)items).GMMDCode);
            if (((DownloadItem*)items).GMMDCode == self.GMMDCode && ((DownloadItem*)items).service_id == _SERVICE_ID) {
                
                flag = YES;
                
            }
           
        }
        
    }
    
    
    if (flag) 
    {
        [appDelegate custonAlert:@"Download" andMassage:@"Now this song downloading."];
    }
    else
    {
        if(![self checkLibraryWithGMMDCode:self.GMMDCode andServiceId:_SERVICE_ID])
        {
            DownloadItem *download = [[DownloadItem alloc] init];
            download.Content = self.Content;
            download.ContentCode = self.ContentCode;
            download.GMMDCode = self.GMMDCode;
            download.sVide = self.sVide;
            download.SongNameEN = self.SongNameEN;
            download.SongNameTH = self.SongNameTH;
            download.thmCover = self.thmCover;
            download.cover = self.cover;
            download.AlbumTH = self.AlbumTH;
            download.ArtistEN = self.ArtistEN;
            download.ArtistTH = self.ArtistTH;
            download.preview = self.preview;
            download.sDuration = self.sDuration;
            download.rbt = self.rbt;
            download.CP_ID = self.CP_ID;
            download.mvCover = self.mvCover;
            download.streaming_mv = self.streaming_mv;
            download.streaming_fs = self.streaming_fs;
            download.lyrics = self.lyrics;
            download.dl_link = _url;
            download.service_id = _SERVICE_ID;
            
            [[DownloadController sharedInstance] addDownloadList:download];
            
            [download resumeInterruptedDownload];
            
            [self totabDownload];
        }
        else
        {
            [appDelegate custonAlert:@"Download" andMassage:@"This song have in storage."];
        }
        

    }
    
}


- (void)postDownloadURLInBackgroundWithType:(NSString *)_SERVICE_ID 
{
//    [appDelegate setMSISDN:@"066818667415"];
//     NSLog(@"msisdn = %@", appDelegate._n);
    NSString* _msisdn = [NSString stringWithFormat:@"%@", [appDelegate MSISDN]];
    NSLog(@"msisdn = %@", appDelegate._n);
    
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/download.jsp?APP_ID=%@&APIVERSION=%@&APPVERSION=%@&DEVICE=%@&GMMD_CODE=%@&SERVICE_ID=%@&MSISDN=%@&CP_ID=%@", APPID, APIVERSION, APPVERSION, appDelegate.deviceId, self.GMMDCode, _SERVICE_ID, _msisdn, self.CP_ID];
    
    NSLog(@"url = %@", URL);
    
    NSURL *pathUrl = [NSURL URLWithString:URL];
    NSLog(@"url = %@", pathUrl);
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:pathUrl];
    request.userInfo = [NSDictionary dictionaryWithObject:_SERVICE_ID forKey:@"service_id"];
    [request setDelegate:self];
    [request startSynchronous];
}

- (void)requestFailed:(ASIHTTPRequest *)request 
{
    NSLog(@"error = %@",[request error]);
    [appDelegate custonAlert:@"Error" andMassage:@"Connection time out. Please try again."];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    NSLog(@">>> requestFinished");
    NSLog(@">>> Res = %@", [request responseString]);
    
    xmlData = [[NSMutableArray alloc] init];
    
    XMLDownload *dlXML = [[XMLDownload alloc]initWithData:[request responseData]];
    NSLog(@">>> xmlData : { %@ }", dlXML.xmlData);
//    NSLog(@">>> service_id : %@", [request.userInfo valueForKey:@"service_id"]);
    
    
    //maz
    if ( [dlXML.xmlData count] > 0 && [[dlXML getStatusCode] isEqualToString:@"200"]) {
        NSLog(@">>> OK dl_link : %@", [[dlXML.xmlData objectAtIndex:0]valueForKey:@"dl_link"]);
        [self addSongToDownloadWithURL:[[dlXML.xmlData objectAtIndex:0]valueForKey:@"dl_link"]andService:[request.userInfo valueForKey:@"service_id"]];
    }
    else
    {
    
//        [appDelegate custonAlert:@"Download" andMassage:@"Please try again."];
//        NSLog(@"Error Detail = %@", [NSString stringWithFormat:@"%@", [[dlXML.xmlData objectAtIndex:0]valueForKey:@"detail"]]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download"
                                                        message:[[dlXML.xmlData objectAtIndex:0]valueForKey:@"detail"]
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
        [alert release];
    }
}

- (void)alertView:(UIAlertView *)alertView  clickedButtonAtIndex:(NSInteger)buttonIndex {
//    if (alertView.tag == myIdentifier)
//        [doSomething];
    NSLog(@"Alert Click");
    [self.view removeFromSuperview];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.tabBarController.tabBar.hidden = YES;
    
    [appDelegate saveProfileWithMSISDN:@"" andCreateDate:nil];
    
    CheckMember *checkMember = [[CheckMember alloc] init];
    [appDelegate.window addSubview:checkMember.view];
}

#pragma mark - ManagedObject
- (BOOL)checkLibraryWithGMMDCode:(NSString *)_gmmdCode andServiceId:(NSString *)_service_id
{
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library"  
											  inManagedObjectContext:appDelegate.managedObjectContext];
	[fetchRequest setEntity:entity];
    
	
    NSString *predicateCondition = [NSString stringWithFormat:@"gmmdcode == %@ AND service_id == %@", _gmmdCode, _service_id];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
    
	NSError *error;
	NSArray *items = [appDelegate.managedObjectContext
					  executeFetchRequest:fetchRequest error:&error];
	
	[fetchRequest release];
	
	if ([items count] > 0) {
		return YES;
	}
	else {
		return NO;
	}
	
}


#pragma mark - Table View Selected


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1) {
    //    if([self isMovingFromParentViewController]){
    //        NSLog(@"ccc");
    //    }
        
        [player pause];
        playing = NO;
    //    NSLog(@"row = %d", indexPath.row);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        SongItem *content = [appDelegate.contentList objectAtIndex:indexPath.row];

        self.Content = content.Content;
        self.ContentCode = content.ContentCode;
        self.GMMDCode = content.GMMDCode;
        self.sVide = content.sVide;
        self.SongNameEN = content.SongNameEN;
        self.SongNameTH = content.SongNameTH;
        self.thmCover = content.thmCover;
        self.cover = content.cover;
        self.AlbumTH = content.AlbumTH;
        self.ArtistEN = content.ArtistEN;
        self.ArtistTH = content.ArtistTH;
        self.preview = content.preview;
        self.sDuration = content.sDuration;
        self.rbt = content.rbt;
        self.CP_ID = content.CP_ID;
        self.mvCover = content.mvCover;
        self.streaming_mv = content.streaming_mv;
        self.streaming_fs = content.streaming_fs;
        self.lyrics = content.lyrics;
        
        self.title =content.SongNameTH;
        [self.tableView setNeedsDisplay];
        [self.tableView setNeedsLayout];
        [self.tableView reloadData];
        [self.tableView setContentOffset:CGPointZero animated:YES];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;    
    }
}


#pragma mark - get song like
- (NSArray *) getSongLike:(NSString *)_gmmdCode {
    
    GetSongLikeRequest *getSongLikeRequest = [[GetSongLikeRequest alloc] init];
    
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/checkSongLike.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&GMMD_CODE=%@&APPVERSION=%@&APIVERSION=%@", APPID, appDelegate.deviceId, @"UTF-8", [appDelegate MSISDN], _gmmdCode, APPVERSION, APIVERSION];
    
    NSURL *pathURL = [NSURL URLWithString:URL];
    NSLog(@"check song like url = %@", pathURL);
    
    request = [ASIHTTPRequest requestWithURL:pathURL];
    [request setDelegate:getSongLikeRequest];
    [request startSynchronous];
    
//    NSLog(@"xxxx >>> %@", getSongLikeRequest);
//    NSLog(@"xxxx >>> %@", getSongLikeRequest.status);
//    NSLog(@"xxxx >>> %@", getSongLikeRequest.statusCode);
//    NSLog(@"xxxx >>> %@", getSongLikeRequest.statusDetail);
//    NSLog(@"xxxx >>> %@", getSongLikeRequest.songLikeCount);
    
    NSArray *array = [NSArray arrayWithObjects:getSongLikeRequest.status, getSongLikeRequest.statusCode, getSongLikeRequest.statusDetail, getSongLikeRequest.songLikeCount, nil];
    
    [getSongLikeRequest release];
    return array;
    
}


#pragma mark - like button action
- (IBAction)btnLikeTouchUpInside:(id)sender {
    
    NSLog(@"TODO :: Likes Action");
    NSLog(@"like status = %@", (boolLikeStatus ? @"YES" : @"NO"));
    
    NSString *status = @"Y";
    
    if(boolLikeStatus == YES){
        status = @"N";
    } else {
        status = @"Y";
    }
    
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/updateSongLike.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&GMMD_CODE=%@&STATUS=%@&APPVERSION=%@&APIVERSION=%@", APPID, appDelegate.deviceId, @"UTF-8", appDelegate.MSISDN, GMMDCode, status, APPVERSION, APIVERSION];
    
    NSURL *pathURL = [NSURL URLWithString:URL];
    NSLog(@"check song like url = %@", pathURL);
    
    GetSongLikeRequest *getSongLikeRequest = [[GetSongLikeRequest alloc] init];
    
    request = [ASIHTTPRequest requestWithURL:pathURL];
    [request setDelegate:getSongLikeRequest];
    [request startSynchronous];
    
    likeStatus = getSongLikeRequest.status;
    likeStatusCode = getSongLikeRequest.statusCode;
    likeStatusDetail = getSongLikeRequest.statusDetail;
    likeCount = getSongLikeRequest.songLikeCount;
    
    if([likeStatusCode isEqualToString:@"502"]){
        boolLikeStatus = YES;
    }else if([likeStatusCode isEqualToString:@"503"]){
        boolLikeStatus = NO;
    }
    
    [self.tableView setNeedsDisplay];
    [self.tableView setNeedsLayout];
    [self.tableView reloadData];
//    SongDetailCell *songDetailCell = [tableView cellForRowAtIndexPath:0];
//    songDetailCell.songLikeCount.text = @"20";
    [getSongLikeRequest release];
    
    
}

- (void) getLikeInfo {
    NSArray *arraySongLike = [self getSongLike:GMMDCode];
    NSLog(@"status = %@", [arraySongLike objectAtIndex:0]);
    NSLog(@"status code = %@", [arraySongLike objectAtIndex:1]);
    NSLog(@"status detail = %@", [arraySongLike objectAtIndex:2]);
    NSLog(@"song like count = %@", [arraySongLike objectAtIndex:3]);
    likeStatus = [arraySongLike objectAtIndex:0];
    likeStatusCode = [arraySongLike objectAtIndex:1];
    likeStatusDetail = [arraySongLike objectAtIndex:2];
    likeCount = [arraySongLike objectAtIndex:3];
}


#pragma mark - twiter share button
- (IBAction)btnTwiterShareTouchUpInside:(id)sender {
    NSLog(@"btnTwiterShareTouchUpInside");
    
    if ([TWTweetComposeViewController class] != nil) {
        if([TWTweetComposeViewController canSendTweet]) {
            NSLog(@"Can send tweet");
            NSURL *url = [[NSURL alloc] initWithString:thmCover];
            NSError *error;
            NSData *data = [[NSData alloc] initWithContentsOfURL:url options:2 error:&error];
            
            
            TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
            [tweetSheet setInitialText:[NSString stringWithFormat:@"โหลดเพลงรอสาย เพลงเต็ม เอ็มวี ริงโทน เพลง %@ เพียงกดโทร *33399", SongNameTH]];
            [tweetSheet addImage:[UIImage imageWithData:data]];
            [tweetSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.333gmmhappy.com/player_full.php?ccode=%@&shelf_id=1", ContentCode]]];
            tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result){
                [self dismissModalViewControllerAnimated:YES];
            };
            [self presentModalViewController:tweetSheet animated:YES];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Support" 
                                                            message:@"Please install Twitter application (Settings > Twitter) and configuration your account. Thank you."
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Support" 
                                                        message:@"This feture not support iOS 4. Please update firmware to iOS 5. Thank you."
                                                       delegate:nil 
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
    
}


#pragma mark - facebook share button
- (IBAction)btnFacebookShareTouchUpInside:(id)sender {
    NSLog(@"btnFacebookShareTouchUpInside");
    facebook = [[Facebook alloc] initWithAppId:@"206603486099478" andDelegate:self];
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"*333 แฮปปี้ได้ไม่อั้น บริการโหลดเพลงยกค่ายแกรมมี่ โหลดเพลงรอสาย เพลงเต็ม เอ็มวี ริงโทน เพลง %@ / %@", SongNameTH, ArtistTH],  @"description",
                                   [NSString stringWithFormat:@"http://www.333gmmhappy.com/player_full.php?ccode=%@&shelf_id=1", ContentCode], @"link",
                                   @"ได้แชร์เพลงผ่าน 333 GMM Happy Application", @"message",
                                   SongNameTH, @"name",
                                   @"http://www.333gmmhappy.com", @"caption",
                                   thmCover, @"picture",
                                   nil];
    
    [facebook dialog:@"feed"
           andParams:params
         andDelegate:self];
}


@end


