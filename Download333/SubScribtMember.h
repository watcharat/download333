//
//  SubScribtMember.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/21/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SubScribtMember : UIViewController {
    AppDelegate *appDelegate;
    ASIHTTPRequest *request;
    NSString *tmpData;
    NSString *STATUS;
    NSString *STATUS_CODE;
    NSString *STATUS_DETAIL;
}

@property (retain, nonatomic) NSString *STATUS;
@property (retain, nonatomic) NSString *STATUS_CODE;
@property (retain, nonatomic) NSString *STATUS_DETAIL;

- (void) alertMessageWithTitle:(NSString*) title andMessage:(NSString*) message;
- (void) connectAPI;

@end
