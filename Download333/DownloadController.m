//
//  DownloadController.m
//  Download333
//
//  Created by Aphinop Supawaree on 12/27/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "DownloadController.h"
#import "DownloadItem.h"
#import "ShelfImageView.h"

static DownloadController *sharedSingletonDelegate = nil;
@implementation DownloadController

@synthesize downloadList;
@synthesize downloadTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark SharedInstance


+ (DownloadController *)sharedInstance {
	@synchronized(self) {
		if (sharedSingletonDelegate == nil) {
			sharedSingletonDelegate = [[self alloc] init]; 
		}
	}
	return sharedSingletonDelegate;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (sharedSingletonDelegate == nil) {
			sharedSingletonDelegate = [super allocWithZone:zone];
			return sharedSingletonDelegate;  
		}
	}
	return nil; 
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    self.downloadTable.backgroundColor = [UIColor clearColor];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
   
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@">>> viewDidAppear");
    NSLog(@">>> downloadList count : %d", [downloadList count]);
    [downloadTable reloadData];
    NSLog(@">>> downloadTable reloadData Complete");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [downloadList count];

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 84;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *DOWNLOAD_CellIdentifier = @"DownloadCell";
        
    DownloadCell *downloadCell = (DownloadCell *)[downloadTable dequeueReusableCellWithIdentifier:DOWNLOAD_CellIdentifier];
    
    if(downloadCell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DownloadCell" owner:self options:nil];
        downloadCell = [nib objectAtIndex:0];
    }

    // Configure the cell...
    DownloadItem *dl = [downloadList objectAtIndex:indexPath.row];
    
    CGRect frame;
    frame.size.width = 64;
    frame.size.height = 64;
    frame.origin.x = 0;
    frame.origin.y = 0;
    ShelfImageView *myImage = [[ShelfImageView alloc] initWithFrame:frame];
    myImage.tag = 1;
    [NSThread detachNewThreadSelector:@selector(thumbnailImage:) toTarget:myImage withObject:[NSString stringWithFormat:@"%@", dl.thmCover]];
    downloadCell.backgroundColor = [UIColor clearColor];
    [downloadCell.imgAlbum addSubview:myImage];
//    NSLog(@">>> dl.SongNameTH : %@ | dl.SongNameEN : %@", dl.SongNameTH, dl.SongNameEN);
    downloadCell.songName.text = dl.SongNameTH;
    [dl.requestDownload setDownloadProgressDelegate:downloadCell.dlProgress];
    [downloadCell.btnCancel setTag:indexPath.row];
    
    return downloadCell;
    
}

- (IBAction)cancelDownload:(id)sender
{
    UIButton *btnDL = (UIButton *)sender; 
    
    [((DownloadItem *)[downloadList objectAtIndex:btnDL.tag]).requestDownload clearDelegatesAndCancel];
    [((DownloadItem *)[downloadList objectAtIndex:btnDL.tag]).requestDownload  removeTemporaryDownloadFile];
    [downloadList removeObjectAtIndex:btnDL.tag];
    
    [self.downloadTable reloadData];
    
}

#pragma mark - Method add DownloadItem
- (int)addDownloadList:(DownloadItem *)download 
{
	if (downloadList == nil) {
		downloadList = [[NSMutableArray alloc] init];
	}
    //download.rowNum = [downloadList count];
	[downloadList addObject:download];
    
    NSLog(@"length = %d", [downloadList count]);
    
	return [downloadList count];
}

@end
