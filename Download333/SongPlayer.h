//
//  SongPlayer.h
//  Download333
//
//  Created by Aphinop Supawaree on 1/2/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "EditPlaylist.h"

@interface SongPlayer : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate *appDelegate;
}

@property (retain, nonatomic) IBOutlet UITableView *tableData;
@property (retain, readwrite) NSString *songIndex;
@property (nonatomic,assign) AVAudioPlayer *audioPlayer;
@property (nonatomic) BOOL playState;
@property (nonatomic) int curIndex;
@property (nonatomic) BOOL isLoop;
@property (nonatomic) BOOL isShuffle;
@property (retain, nonatomic) NSArray *songList;
@property (nonatomic, strong) NSString *currentPlayingGmmdCode;

+ (SongPlayer *)sharedInstance;

- (NSString *)getSongFilePath:(NSString *)nameOfFile;

- (IBAction)PlayPause:(id)sender;
- (IBAction)nextSong:(id)sender;
- (IBAction)prevSong:(id)sender;
- (IBAction)shuffleSong:(id)sender;
- (IBAction)repeatSong:(id)sender;
- (IBAction)updateTracker:(id)sender;

- (void)playSongs;
- (void)PlayWhenYesPlayState;
- (void)pauseSong;
- (void)nextSongs;
- (void)prevSongs;
- (void)setRepeat;
- (void)setShuffle;
- (void)shuffle;
- (void)searchCurrentIndexWith:(NSArray *)array andGMMDCode:(NSString *)_gmmdcode;

- (void)changeBtnPlayer;
- (void) listSong;
- (void) resetAfterEdit;

@end
