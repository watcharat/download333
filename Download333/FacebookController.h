//
//  FacebookController.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/18/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacebookController : UIViewController {
    
    IBOutlet UIWebView *facebookWebView;
    
}

@property (nonatomic, retain) UIWebView *facebookWebView;

@end
