//
//  SearchNavigationButton.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/5/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "SearchNavigationButton.h"

@implementation SearchNavigationButton

-(id)init {
    
    if(self = [super init]) {
        self.frame = CGRectMake(0, 0, 31.0, 31.0);
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        UIImage *image = [UIImage imageNamed:@"bt_icon_search.png"];
        
        UIImage *stretchImage = [image stretchableImageWithLeftCapWidth:2.0 topCapHeight:2.0];
        
        [self setBackgroundImage:stretchImage forState:UIControlStateNormal];
        
        self.backgroundColor = [UIColor clearColor];
        
        [self setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
//        self.titleShadowOffset = CGSizeMake(0, -1);
//        self.font = [UIFont boldSystemFontOfSize:13];
    }
    
    return self;
}

@end
