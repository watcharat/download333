//
//  EditPlaylist.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/20/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "EditPlaylist.h"

@implementation EditPlaylist

@synthesize tableView;
@synthesize songList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    self.tableView.backgroundColor = [UIColor clearColor];
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [self.navigationItem setTitle:@"Edit"];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donNavButtonPressed)] autorelease];
    tableView.editing = YES;
}

- (IBAction)donNavButtonPressed {
    NSLog(@"DONE PLAYLIST");
    if(appDelegate.playlistView != NULL){
        if([appDelegate.playlistView parentViewController] != NULL){
            [appDelegate.playlistView popViewControllerAnimated:YES];
        }
    }
}

- (void)viewDidUnload
{
    [tableView release];
    tableView = nil;
    [super viewDidUnload];

}

-(void)viewWillAppear:(BOOL)animated{
    
    NSLog(@">>> viewWillAppear");
    firstLoad = YES;
    [self listSong];
    [self.tableView reloadData];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"xxxx = %d", [self.songList count]);
//    return 0;
    return [self.songList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"PlayerPlaylistSongCellIdentifier";
    
    PlayerPlaylistSongCell *Cell = (PlayerPlaylistSongCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(Cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlayerPlaylistSongCell" owner:self options:nil];
        Cell = [nib objectAtIndex:0];
    }
        
    [Cell.imgBG setAlpha:1.0];
    
    Cell.imgSong.image = [UIImage imageWithData:[[appDelegate.playlistSongList objectAtIndex:indexPath.row]valueForKey:@"thmcover"]];
    Cell.lblSongName.text = [[appDelegate.playlistSongList objectAtIndex:indexPath.row]valueForKey:@"songname_th"];
    Cell.lblArtistName.text = [[appDelegate.playlistSongList objectAtIndex:indexPath.row]valueForKey:@"artist_name"];
    return Cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 78;
}


-(void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSLog(@"indexPath.section = %d", indexPath.section);
        NSString *gmmdCode = [[self.songList objectAtIndex:indexPath.row]valueForKey:@"gmmdcode"];
        NSLog(@"gmmdCode = %@", gmmdCode);
        

        NSLog(@"####currentPlayingGmmdCode = %@", [[SongPlayer sharedInstance] currentPlayingGmmdCode]);
        if([gmmdCode isEqualToString:[[SongPlayer sharedInstance] currentPlayingGmmdCode]]){
            [[SongPlayer sharedInstance] changeBtnPlayer];
            [[[SongPlayer sharedInstance] audioPlayer] stop];
        }
        
        
        NSLog(@"todo clear all music from library");
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSString *predicateCondition = [NSString stringWithFormat:@"service_id = 1 AND gmmdcode = '%@'", gmmdCode];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
        [fetchRequest setPredicate:predicate];
        
        NSError *error;
        NSArray *items = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        [fetchRequest release];
        
        if([items count]){
            NSLog(@">>>> %d", [items count]);
            if([items count] == 1){
                [[SongPlayer sharedInstance] resetAfterEdit];
            }
            for (Library *lib in items) {
                lib.inPlaylist = @"NO";
            }
        }
        
        [appDelegate.managedObjectContext save:&error];
        [self listSong];
        [self.tableView reloadData];
        
    }
}


#pragma mark - get song data
#pragma - mark ManagedObject
- (void)listSong
{
	NSLog(@"todo listSong");
    if(firstLoad) {
        NSLog(@"firstLoad = YES");
        self.songList = appDelegate.playlistSongList;
        firstLoad = NO;
    }else{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
        
        [fetchRequest setResultType:NSDictionaryResultType];
        
        [fetchRequest setEntity:entity];
        
        NSString *predicateCondition = [NSString stringWithFormat:@"service_id = 1 AND inPlaylist = 'YES'"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
        [fetchRequest setPredicate:predicate];
        
        
        NSError *error = nil;
        NSArray *results = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        
        [fetchRequest release];
        
        if ([results count]) {
            
            self.songList = [NSArray arrayWithArray:results];
        }
        else
        {
            self.songList = nil;
        }
        
        appDelegate.playlistSongList = self.songList;
    }
}


#pragma mark - clear all click
- (IBAction)btnClearAllTUI:(id)sender {
    NSLog(@"clear all touch up inside");   
    [[SongPlayer sharedInstance] changeBtnPlayer];
    [[[SongPlayer sharedInstance] audioPlayer] pause];
    [self deleteFromLibrary];
    [self listSong];
    [self.tableView reloadData];
}


#pragma mark - clear music from library
- (void) deleteFromLibrary {
    NSLog(@"todo clear all music from library");
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSString *predicateCondition = [NSString stringWithFormat:@"service_id = 1"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *items = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    
    if([items count]){
        for (Library *lib in items) {
            lib.inPlaylist = @"NO";
        }
    }
    
    if (![appDelegate.managedObjectContext save:&error]) {
        
    }
    
}


- (void)dealloc {
    [tableView release];
    [super dealloc];
}


@end




