//
//  CategoryListController.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "CategoryListController.h"
#import "AFXMLRequestOperation.h"
@implementation CategoryListController

@synthesize shelfID;

@synthesize tableView;
@synthesize shelfLabelItem;
@synthesize controller;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    self.tableView.backgroundColor = [UIColor clearColor];
    appDelegate = [(AppDelegate *)[UIApplication sharedApplication] delegate];
    controller = [[NSMutableArray alloc] init];
    
    // ---- Setting Navigate Right Button
    SearchNavigationButton *btnSearch = [[SearchNavigationButton alloc] init];
    [btnSearch addTarget:self action:@selector(Search:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:btnSearch];
    self.navigationItem.rightBarButtonItem = button;
    // ---- End Setting Navigate Right Button
    
    // ---- Setting Navigate Left Button
    BackNavigateButton *btnBackView = [[BackNavigateButton alloc] init];
    [btnBackView addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithCustomView:btnBackView];
    self.navigationItem.leftBarButtonItem = btnBack;
    // ---- End Setting Navigate Left Button
    
    self.navigationItem.title = shelfID;
    
    [self connectAPI];
    
}


- (void)Back:(id)sender {
    
    if([[self.navigationController childViewControllers] count] >0 ){
        [self.navigationController popViewControllerAnimated:YES]; 
    }
    

}

- (void)viewDidUnload
{
    [tableView release];
    tableView = nil;
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [tmpData release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [tableView release];
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [controller count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ShelfLabelCellIdentifier = @"ShelfLabelCellIdentifier";
    ShelfLabelCell *shelfLabelCell = (ShelfLabelCell *) [tableView dequeueReusableCellWithIdentifier: ShelfLabelCellIdentifier];
    
    if(shelfLabelCell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShelfLabelCell" owner:self options:nil];
        shelfLabelCell = [nib objectAtIndex:0];
    }
    
    //    UIImage *_image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[(ShelfLabelController *)[controller objectAtIndex:indexPath.row] SHELF_MENU_IMAGE_OFF]]]];
    
    //    shelfLabelCell.shelfLabelImage.image = _image;
    //
    //    
    //    [_image release];
    CGRect frame;
    frame.size.width = 298;
    frame.size.height = 50;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [[shelfLabelCell.contentView viewWithTag:1] removeFromSuperview];
    ShelfImageView *myImage = [[ShelfImageView alloc] initWithFrame:frame];
    myImage.tag = 1;
    [NSThread detachNewThreadSelector:@selector(thumbnailImage:)
                             toTarget:myImage
                           withObject:[NSString stringWithFormat:@"%@", [(ShelfLabelController *)[controller objectAtIndex:indexPath.row] SHELF_MENU_IMAGE_OFF]]];
    
    [shelfLabelCell.shelfLabelImage addSubview:myImage];
    shelfLabelCell.backgroundColor = [UIColor clearColor];
    [myImage release];
    
    return shelfLabelCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 45.0;
}

#pragma mark - Table View Selected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [appDelegate.contentList removeAllObjects];
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSUInteger row = [indexPath row];
            
    ShelfLabelController *nextController = [[ShelfLabelController alloc] initWithNibName:@"SongListView" bundle:[NSBundle mainBundle]];
        
    nextController.ID = [[controller objectAtIndex:row] valueForKey:@"ID"];
    nextController.NAMETH = [[controller objectAtIndex:row] valueForKey:@"NAMETH"];
    nextController.NAMEEN = [[controller objectAtIndex:row] valueForKey:@"NAMEEN"];
    nextController.DESC = [[controller objectAtIndex:row] valueForKey:@"DESC"];
    nextController.TYPE = [[controller objectAtIndex:row] valueForKey:@"TYPE"];
    nextController.LEVEL = [[controller objectAtIndex:row] valueForKey:@"LEVEL"];
    nextController.SHELF_ID = [[controller objectAtIndex:row] valueForKey:@"SHELF_ID"];
    nextController.SHELF_ICON = [[controller objectAtIndex:row] valueForKey:@"SHELF_ICON"];
    nextController.SHELF_MENU_IMAGE_ON = [[controller objectAtIndex:row] valueForKey:@"SHELF_MENU_IMAGE_ON"];
    nextController.SHELF_MENU_IMAGE_OFF = [[controller objectAtIndex:row] valueForKey:@"SHELF_MENU_IMAGE_OFF"];
    nextController.searchString = @"";
        
    [appDelegate.mainViewController pushViewController:nextController animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [nextController release];
        
    
    
}


#pragma mark - Connect API
- (void) connectAPI {
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/shelfLabel.jsp?APP_ID=%@&DEVICE=%@&APIVERSION=%@&SHELF_ID=%@&MSISDN=%@&APPVERSION=%@", APPID, appDelegate.deviceId, APIVERSION, shelfID, appDelegate.MSISDN, APPVERSION];
    
    NSURL *pathUrl = [NSURL URLWithString:URL];
//    NSLog(@"url = %@", pathUrl);
//    
//    request = [ASIHTTPRequest requestWithURL:pathUrl];
//    [request setDelegate:self];
//    [request startSynchronous];
    NSURLRequest *request = [NSURLRequest requestWithURL:pathUrl];
    AFXMLRequestOperation *operation = [AFXMLRequestOperation XMLParserRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSXMLParser *XMLParser) {
        shelfArray = [[NSMutableArray alloc] init];
        XMLParser.delegate = self;
        [XMLParser parse];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSXMLParser *XMLParse) {
        NSLog(@"error = %@",error);
    }];
    [operation start];
}

- (void)requestStarted:(ASIHTTPRequest *)request {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"request start");
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"request finish");
    shelfArray = [[NSMutableArray alloc] init];
    NSString *respString = [request responseString];
    //    NSLog(@"%@", respString);
    NSData *respData = [respString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData: respData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"error = %@",[request error]);
    [appDelegate custonAlert:@"Error" andMassage:@"Connection time out. Please try again."];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if(qName) {
        elementName = qName;
    }
    
    if([elementName isEqualToString:@"SHELFLABEL"]) {
        if(!self.shelfLabelItem){
            self.shelfLabelItem = [[ShelfLabelItem alloc] init];
        }
    }else{
        if(!tmpData){
            tmpData = [[NSMutableString alloc] init];
        }
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {    
    //    tmpData = string;
    if (tmpData) {
        [tmpData appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {   
    
    if(qName) {
        elementName = qName;
    }
    
    if([elementName isEqualToString:@"SHELFLABEL"]) {
        [shelfArray addObject:shelfLabelItem];
//        [shelfLabelItem release];
        shelfLabelItem = nil;
    } else if([elementName isEqualToString:@"ID"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"NAMETH"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"NAMEEN"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"DESC"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"TYPE"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"LEVEL"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"SHELF_ID"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"SHELF_ICON"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"SHELF_MENU_IMAGE_ON"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else if ([elementName isEqualToString:@"SHELF_MENU_IMAGE_OFF"]) {
        [shelfLabelItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
//        [tmpData release];
        tmpData = nil;
    } else {
//        [tmpData release];
        tmpData = nil;
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    
    
    ShelfLabelController *shelfLabelController;
    
    for (ShelfLabelItem *item in shelfArray) {
        shelfLabelController = [[ShelfLabelController alloc] init];
        shelfLabelController.ID = item.ID;
        shelfLabelController.NAMETH = item.NAMETH;
        shelfLabelController.NAMEEN = item.NAMEEN;
        shelfLabelController.DESC = item.DESC;
        shelfLabelController.TYPE = item.TYPE;
        shelfLabelController.LEVEL = item.LEVEL;
        shelfLabelController.SHELF_ID = item.SHELF_ID;
        shelfLabelController.SHELF_ICON = item.SHELF_ICON;
        shelfLabelController.SHELF_MENU_IMAGE_ON = item.SHELF_MENU_IMAGE_ON;
        shelfLabelController.SHELF_MENU_IMAGE_OFF = item.SHELF_MENU_IMAGE_OFF;
        [self.controller addObject:shelfLabelController];
        [shelfLabelController release];
        shelfLabelController = nil;
        
    }
    
    [self.tableView setNeedsDisplay];
    [self.tableView setNeedsLayout];
    [self.tableView reloadData];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}

- (IBAction)Search:(id)sender {
    NSLog(@"Search Button Touch Up Inside");
    SearchView *search = [[SearchView alloc] init];
    [appDelegate.mainViewController pushViewController:search animated:YES];
    [search release];
}


@end


