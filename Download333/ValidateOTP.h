//
//  ValidateOTP.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/19/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "CheckMember.h"

@interface ValidateOTP : UIViewController <UITextFieldDelegate,NSXMLParserDelegate> {
    AppDelegate *appDelegate;
    NSString *tmpData;

}

@property (retain, nonatomic) IBOutlet UITextField *txtValidateCode;

@property (strong, nonatomic) NSString *STATUS;
@property (strong, nonatomic) NSString *STATUS_CODE;
@property (strong, nonatomic) NSString *STATUS_DETAIL;

- (void) alertMessageWithTitle:(NSString*) title andMessage:(NSString*) message;
- (void) connectAPIWithMSISDN:(NSString *) msisdnRQ andValidateCode:(NSString *) validateCodeRQ;


@end
