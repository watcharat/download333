//
//  LibrarySongMainController.h
//  Download333
//
//  Created by Aphinop Supawaree on 12/31/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface LibrarySongMainController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate *appDelegate;
}

@property (retain, nonatomic) IBOutlet UITableView *tableData;
@property (retain, nonatomic) NSArray *artistList;
@property (retain, nonatomic) NSArray *songOfArtistList;

- (void)listArtistName;
- (void)deleteFromLibraryWithArtistName:(NSString *)_artist_name;
- (NSString *)getSongFilePath:(NSString *)nameOfFile;
- (void)listSongWithArtistName:(NSString *)_artist_name;

@end
