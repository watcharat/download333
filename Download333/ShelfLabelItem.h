//
//  ShelfLabelItem.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/23/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShelfLabelItem : NSObject {
    NSString *SHELFLABEL;
    NSString *ID;
    NSString *NAMETH;
    NSString *NAMEEN;
    NSString *DESC;
    NSString *TYPE;
    NSString *LEVEL;
    NSString *SHELF_ID;
    NSString *SHELF_ICON;
    NSString *SHELF_MENU_IMAGE_ON;
    NSString *SHELF_MENU_IMAGE_OFF;
}

@property (nonatomic, retain) NSString *SHELFLABEL;
@property (nonatomic, retain) NSString *ID;
@property (nonatomic, retain) NSString *NAMETH;
@property (nonatomic, retain) NSString *NAMEEN;
@property (nonatomic, retain) NSString *DESC;
@property (nonatomic, retain) NSString *TYPE;
@property (nonatomic, retain) NSString *LEVEL;
@property (nonatomic, retain) NSString *SHELF_ID;
@property (nonatomic, retain) NSString *SHELF_ICON;
@property (nonatomic, retain) NSString *SHELF_MENU_IMAGE_ON;
@property (nonatomic, retain) NSString *SHELF_MENU_IMAGE_OFF;

@end
