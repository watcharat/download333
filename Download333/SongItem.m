//
//  SongItem.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "SongItem.h"

@implementation SongItem


@synthesize Content;
@synthesize ContentCode;
@synthesize GMMDCode;
@synthesize sVide;
@synthesize SongNameEN;
@synthesize SongNameTH;
@synthesize thmCover;
@synthesize cover;
@synthesize AlbumTH;
@synthesize ArtistEN;
@synthesize ArtistTH;
@synthesize preview;
@synthesize sDuration;
@synthesize rbt;
@synthesize CP_ID;
@synthesize mvCover;
@synthesize streaming_mv;
@synthesize streaming_fs;
@synthesize lyrics;

- (void) dealloc {
    [Content release];
    [ContentCode release];
    [GMMDCode release];
    [sVide release];
    [SongNameEN release];
    [SongNameTH release];
    [thmCover release];
    [cover release];
    [AlbumTH release];
    [ArtistEN release];
    [ArtistTH release];
    [preview release];
    [sDuration release];
    [rbt release];
    [CP_ID release];
    [mvCover release];
    [streaming_mv release];
    [streaming_fs release];
    [lyrics release];
}

@end
