//
//  OTP.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/14/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ValidateOTP.h"

@interface OTP : UIViewController<UITextFieldDelegate,NSXMLParserDelegate> {
    AppDelegate *appDelegate;
    NSString *tmpData;
//    NSString *STATUS;
//    NSString *STATUS_CODE;
//    NSString *STATUS_DETAIL;
//    NSString *MSISDNRQ;
}

- (void) alertMessageWithTitle:(NSString*) title andMessage:(NSString*) message;
- (void) connectAPI:(NSString *) msisdnRQ;

@property (retain, nonatomic) IBOutlet UITextField *textfieldPhoneNumber;

@property (retain, nonatomic) IBOutlet UITextField *textField;
@property (retain, nonatomic) IBOutlet UIButton *closeBtn;
@property (retain, nonatomic) IBOutlet UIButton *submitBtn;

@property (nonatomic, strong) NSString *STATUS;
@property (nonatomic, strong) NSString *STATUS_CODE;
@property (nonatomic, strong) NSString *STATUS_DETAIL;
@property (nonatomic, strong) NSString *MSISDNRQ;

@end
