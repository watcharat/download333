//
//  AppDelegate.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/9/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "GetMSISDN.h"
#import "ASIHTTPRequest.h"
#import "PROFILE.h"
#import "OTP.h"
#import "Exit.h"


#import "AppleMSISDN.h"

//#define appDelegate.deviceId [[UIDevice currentDevice] uniqueIdentifier]
#define APPID @"175"
#define APIVERSION @"2.0.0"
#define APPVERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]

@interface AppDelegate : UIResponder <UIApplicationDelegate, ASIHTTPRequestDelegate>{
    BOOL internetConnection;
    BOOL wifiConnection;
    NSString *MSISDN;
    NSString *OPERATOR;
    
    IBOutlet UINavigationController *mainViewController;
    IBOutlet UINavigationController *playlistView;
    
    NSMutableDictionary *thumbnailCache;
    
    NSMutableArray *contentList;
    
    NSString *songPlayingGmmdCode;
    
    
}

@property (nonatomic, retain) NSString *deviceId;

@property (nonatomic, retain) UINavigationController *mainViewController;
@property (nonatomic, retain) UINavigationController *playlistView;

@property (strong, nonatomic) IBOutlet UIWindow *window;

@property (strong, nonatomic) IBOutlet UITabBarController *mainTab;
@property (retain, nonatomic) IBOutlet UITabBarController *playListTab;

@property NetworkStatus remoteHostStatus;
@property NetworkStatus internetConnectionStatus;
@property NetworkStatus localWiFiConnectionStatus;

// CORE DATA
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

// MSISDN
@property (nonatomic, retain) NSString *MSISDN;
@property (nonatomic, retain) NSString *OPERATOR;
@property (nonatomic, retain) NSString *_n;

@property (nonatomic,retain) NSMutableDictionary *thumbnailCache;

// Player
@property (strong ,readwrite) NSArray *playlistSongList;
@property (strong, readwrite) NSMutableArray *playlistSongDefault;
@property (strong, readwrite) NSMutableArray *playlistSongShuffle;
@property (nonatomic, readwrite) BOOL playState;

// Content List
@property (nonatomic, retain) NSMutableArray *contentList;

@property (nonatomic, retain) NSString *songPlayingGmmdCode;

// DETECT INTERNET CONNECTION METHOD
- (BOOL) detectInternetConnection;
- (BOOL) detectWifiConnection;

 
// ALERT
- (void) custonAlert : (NSString *)title andMassage:(NSString *)_msg;

// SAVE OTP DATA
- (void) saveProfileWithMSISDN:(NSString *) msisdn andCreateDate:(NSDate *) createDate;

// Exit View
- (void) exitView;

@end
