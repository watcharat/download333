//
//  GetSongLikeRequest.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/4/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "GetSongLikeRequest.h"

@implementation GetSongLikeRequest

@synthesize status, statusCode, statusDetail, songLikeCount;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSLog(@"error = %@",[request error]);
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    NSString *respString = [request responseString];
//    NSLog(@"%@", respString);
    NSData *respData = [respString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:respData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
//    if(!tmpData){
//        tmpData = [[NSMutableString alloc] init];
//    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {    
//    tmpData = string;
    if (!tmpData) {
        tmpData = [[NSMutableString alloc] init];
        [tmpData appendString:string];
    }
    NSLog(@">>string >>%@", string);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {     
    
    if([elementName isEqualToString:@"STATUS"]) {
        self.status = [NSString stringWithFormat:@"%@", tmpData];
        [tmpData release];
        tmpData = nil;
    }else if([elementName isEqualToString:@"STATUS_CODE"]){
        self.statusCode = [NSString stringWithFormat:@"%@", tmpData];
        [tmpData release];
        tmpData = nil;
    }else if([elementName isEqualToString:@"STATUS_DETAIL"]){
        self.statusDetail = [NSString stringWithFormat:@"%@", tmpData];
        [tmpData release];
        tmpData = nil;
    }else if([elementName isEqualToString:@"SONG_LIKE_COUNT"]){
        self.songLikeCount = [NSString stringWithFormat:@"%@", tmpData];
        [tmpData release];
        tmpData = nil;
    }else{
        [tmpData release];
        tmpData = nil;
    }
    
    
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"status code = %@", self.statusCode);
    NSLog(@"song like count = %@", self.songLikeCount);
}

@end
