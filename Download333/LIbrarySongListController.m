//
//  LIbrarySongListController.m
//  Download333
//
//  Created by Aphinop Supawaree on 1/1/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "LIbrarySongListController.h"
#import "LibrarySongCell.h"
#import "Library.h"

@implementation LIbrarySongListController
@synthesize tableData;
@synthesize searchLabel;
@synthesize songList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self.navigationItem setTitle:self.searchLabel];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.tableData setBackgroundColor:[UIColor clearColor]];
    [self.tableData setSeparatorColor:[UIColor clearColor]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    
    [self listSong];
    [self.tableData reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@">>> songList : %@", self.songList);
    
    return [self.songList count];

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 78;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"LibrarySongCellIdentifier";
    
    LibrarySongCell *Cell = (LibrarySongCell *)[tableData dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(Cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LibrarySongCell" owner:self options:nil];
        Cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
//    [menuButton addTarget:self action:@selector(mainMenu_Clicked_back:) forControlEvents:UIControlEventTouchUpInside];
    
    [Cell.btnAdd setTag:indexPath.row];
    Cell.backgroundColor = [UIColor clearColor];
    NSString *inPlaylist = [NSString stringWithString:[[self.songList objectAtIndex:indexPath.row]valueForKey:@"inPlaylist"]];
    NSLog(@"indexPath.row : %d | inPlaylist : %@", indexPath.row, inPlaylist);
    if ([inPlaylist isEqualToString:@"NO"]) {
        [Cell.btnAdd setImage:[UIImage imageNamed:@"bt_icon_add.png"] forState:UIControlStateNormal];
        [Cell.btnAdd setTitle:@"YES" forState:UIControlStateNormal];
    }
    else
    {
        [Cell.btnAdd setImage:[UIImage imageNamed:@"bt_icon_select.png"] forState:UIControlStateNormal];
        [Cell.btnAdd setTitle:@"NO" forState:UIControlStateNormal];
    }
    
    Cell.imgThmCover.image = [UIImage imageWithData:[[self.songList objectAtIndex:indexPath.row]valueForKey:@"thmcover"]];
    Cell.lblSongName.text = [[self.songList objectAtIndex:indexPath.row]valueForKey:@"songname_th"];
    Cell.lblArtistname.text = [[self.songList objectAtIndex:indexPath.row]valueForKey:@"artist_name"];
    
    return Cell;
    
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing: editing animated: animated];
    [self.tableData setEditing:editing animated:animated];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		@try {
            NSLog(@">>> Del");
            
            NSString *s_path = [NSString stringWithString:[self getSongFilePath:[[self.songList objectAtIndex:indexPath.row]valueForKey:@"gmmdcode"]]];
            if ([[NSFileManager defaultManager] fileExistsAtPath:s_path]) {
                [[NSFileManager defaultManager] removeItemAtPath:s_path error:nil];
                
            }
            
            [self deleteFromLibraryWithGMMDCode:[[self.songList objectAtIndex:indexPath.row]valueForKey:@"gmmdcode"]];
            
            

		}
		@catch (NSException *ex) {
			NSLog(@">>> errDel : %@", ex);
		}
        
        [self listSong];
        
        NSLog(@">>> [self.songLis count] : %d", [self.songList count]);
        
        [self.tableData reloadData];
	}
}

#pragma - mark Table View Did Select Row
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    NSLog(@"row = %d", row);
    NSString *inPlaylist = [NSString stringWithString:[[self.songList objectAtIndex:row]valueForKey:@"inPlaylist"]];
    NSLog(@"inplaylist = %@", inPlaylist);
    
    if ([inPlaylist isEqualToString:@"NO"]) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" 
                                                  inManagedObjectContext:appDelegate.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSString *predicateCondition = [NSString stringWithFormat:@"gmmdcode == '%@' AND service_id = 1", [[self.songList objectAtIndex:row]valueForKey:@"gmmdcode"]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
        [fetchRequest setPredicate:predicate];
        
        NSError *error;
        NSArray *items = [appDelegate.managedObjectContext
                          executeFetchRequest:fetchRequest error:&error];
        
        [fetchRequest release];
        
        if ([items count]) {
            
            for (Library *libItems in items) {
                libItems.inPlaylist = @"YES";
            }
            
        }
        
        [appDelegate.managedObjectContext save:nil];
        
        [self listSong];
        [self listSongInPlaylist];
        
    } else {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" 
                                                  inManagedObjectContext:appDelegate.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSString *gmmdCode = [[self.songList objectAtIndex:row]valueForKey:@"gmmdcode"];
        
        NSString *predicateCondition = [NSString stringWithFormat:@"gmmdcode == '%@' AND service_id = 1", gmmdCode];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
        [fetchRequest setPredicate:predicate];
        
        NSError *error;
        NSArray *items = [appDelegate.managedObjectContext
                          executeFetchRequest:fetchRequest error:&error];
        
        [fetchRequest release];
        
        if([gmmdCode isEqualToString:appDelegate.songPlayingGmmdCode]){
            [[SongPlayer sharedInstance] changeBtnPlayer];
            [[[SongPlayer sharedInstance] audioPlayer] stop];
        }
        
        
        if ([items count]) {
            
            for (Library *libItems in items) {
                libItems.inPlaylist = @"NO";
            }
            
        }
        
        [appDelegate.managedObjectContext save:nil];
        
        [self listSong];
        
        [self listSongInPlaylist];
    }
    [self.tableData reloadData];
}

#pragma - mark ManagedObject
- (void)listSong
{
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    
    [fetchRequest setResultType:NSDictionaryResultType];
    
	[fetchRequest setEntity:entity];
	
    NSString *predicateCondition = [NSString stringWithFormat:@"artist_name == '%@' AND service_id == 1", self.searchLabel];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
	
    
    NSError *error = nil;
    NSArray *results = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
	
	[fetchRequest release];
	
	if ([results count]) {
        
        self.songList = [NSArray arrayWithArray:results];
	}
    else
    {
        self.songList = nil;
    }
	
}

#pragma - mark ManagedObject
- (void)listSongInPlaylist
{
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    
    [fetchRequest setResultType:NSDictionaryResultType];
    
	[fetchRequest setEntity:entity];
	
    NSString *predicateCondition = [NSString stringWithFormat:@"service_id == 1 AND inPlaylist = 'YES'"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
	
    
    NSError *error = nil;
    NSArray *results = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
	
	[fetchRequest release];
	
	if ([results count]) {
        appDelegate.playlistSongList = [NSArray arrayWithArray:results];
	}
    else
    {

    }
    
	
}

- (IBAction)insertSongToPlaylist:(id)sender
{
 
    UIButton *btn = (UIButton *)sender;
    NSLog(@"btn.tag : %d | btn.titleLabel.text : %@ | btn.gmmdcode : %@", btn.tag, btn.titleLabel.text, [[self.songList objectAtIndex:btn.tag]valueForKey:@"gmmdcode"]);
    
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" 
											  inManagedObjectContext:appDelegate.managedObjectContext];
	[fetchRequest setEntity:entity];
	
    NSString *predicateCondition = [NSString stringWithFormat:@"gmmdcode == '%@' AND service_id = 1", [[self.songList objectAtIndex:btn.tag]valueForKey:@"gmmdcode"]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
	
	NSError *error;
	NSArray *items = [appDelegate.managedObjectContext
					  executeFetchRequest:fetchRequest error:&error];
	
	[fetchRequest release];
	
	if ([items count]) {
        
        for (Library *libItems in items) {
            libItems.inPlaylist = btn.titleLabel.text;
        }
        
	}
    
    [appDelegate.managedObjectContext save:nil];
    
    [self listSong];
	[self.tableData reloadData];
    
}

- (void)deleteFromLibraryWithGMMDCode:(NSString *)_gmmdcode
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSString *predicateCondition = [NSString stringWithFormat:@"gmmdcode = %@ AND service_id = 1",_gmmdcode];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *items = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    
    if([_gmmdcode isEqualToString:[[SongPlayer sharedInstance] currentPlayingGmmdCode]]){
        [[SongPlayer sharedInstance] changeBtnPlayer];
        [[[SongPlayer sharedInstance] audioPlayer] stop];
    }
        
    
    for (NSManagedObject *managedObject in items) {
        [appDelegate.managedObjectContext deleteObject:managedObject];
        //        NSLog(@"%@ object deleted",entityname);
    }
    if (![appDelegate.managedObjectContext save:&error]) {
        //        NSLog(@"Error deleting %@ - error:%@",entityname,error);
    }
}

#pragma mark - song path
- (NSString *)getSongFilePath:(NSString *)nameOfFile
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);

//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *filename = [[NSString alloc]initWithFormat:@"%@.mp3",nameOfFile];
    NSString *uniquePath = [documentDirectory stringByAppendingPathComponent:filename];
    
    return uniquePath;
    
}

@end
