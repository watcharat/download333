//
//  LibraryMVListController.h
//  Download333
//
//  Created by Aphinop Supawaree on 1/1/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface LibraryMVListController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate *appDelegate;
}

@property (retain, nonatomic) IBOutlet UITableView *tableData;
@property (retain, nonatomic) NSArray *songList;

- (void)listSong;
- (IBAction)insertSongToPlaylist:(id)sender;
- (void)deleteFromLibraryWithGMMDCode:(NSString *)_gmmdcode;

- (NSString *)getSongFilePath:(NSString *)nameOfFile;

-(void)layoutSubviews:(BOOL)editing;

@end
