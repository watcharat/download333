//
//  XMLDownload.m
//  ArtistApp
//
//  Created by  on 9/7/54 BE.
//  Copyright 2554 __MyCompanyName__. All rights reserved.
//

#import "XMLDownload.h"

@implementation XMLDownload
@synthesize xmlData;
@synthesize drRecord;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
//         ArtistAppAppDelegate *appDelegate = (ArtistAppAppDelegate *)[[UIApplication sharedApplication]delegate]; 
//        NSString *path = [NSString stringWithFormat:@"http://directory.gmmwireless.com/artistmusic/api/download.jsp?APP_ID=3&DEVICE=xxx&GMMD_CODE=1004454801&SERVICE_ID=41&REFID=&MSISDN=7&ALBUM_ID=6987"];
//        NSLog(@"path : %@", path);
//        [self parseXMLFileAtURL:path];

    }
    
    return self;
}


-(id)initWithData:(NSData *)dataXml
{
    self = [super init];
    if (self) {
        // Initialization code here.
        if ([xmlData count] == 0) {
            [self parseXMLData:dataXml];
        }
        
    }
    
    return self;
}

-(NSString *)getStatusCode{
    
    NSString *statusCode = [[NSString alloc]init];
    for (NSArray *valXml in xmlData) {
        statusCode = [valXml valueForKeyPath:@"status_code"];
    } 
    
    if ([statusCode isEqualToString:@""]) {
        statusCode = @"";
    }
    
    return statusCode;
    
}

//-(void)checkDLServiceByStatusCode:(NSString *)statusCode andMsisdn:(NSString *)msisdn andAlbumId:(NSString *)albumId{
//    
//    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate]; 
//    
//    NSMutableArray *dlList = [[NSMutableArray alloc] initWithArray:appDelegate.downloadList];
//    
//    NSString *validateDetail = [[NSString alloc]init];
//    NSString *dl_link = [[NSString alloc]init];
//    
//    drRecord = FALSE;
//    
//    if ([statusCode intValue] == 102) {
//        [xmlServiceObj charging:msisdn andAlbumId:albumId]; 
//        [xmlServiceObj checkCharging];
//    }else if ([statusCode intValue] == 101) {
//        
//        for (NSArray *valDetailXml in xmlData) {
//            validateDetail = [valDetailXml valueForKeyPath:@"detail"];
//        }
//        
//        UIAlertView *errView = [[UIAlertView alloc] initWithTitle:@"OTP Alert" message:validateDetail delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        
//        
//        errView.delegate = appDelegate;
//        [errView show];
//        [errView release];
//        
//    }else if([statusCode intValue] == 200){
//        for (NSArray *valDetailXml in xmlData) {
//            dl_link = [valDetailXml valueForKeyPath:@"dl_link"];
//        }
//        NSLog(@"DL Link : %@",dl_link);
//        
//        [dlList addObject:appDelegate.nowGmmdCode];
//        
//        appDelegate.downloadList = dlList;
//        
//        NSLog(@"appDelegate.downloadList : %@", appDelegate.downloadList);
//    }else if ([statusCode intValue] == 201) {
//        NSLog(@"DR recorded");
//        drRecord = TRUE;
//    }
//    
//}

// Xml parse
#pragma mark - parse XML

- (void)parseXMLData: (NSData *)dataXML{
    xmlData = [[NSMutableArray alloc] init]; 
    xmlParser = [[NSXMLParser alloc] initWithData:dataXML];
    [xmlParser setDelegate:self];
    [xmlParser parse];
}
//- (void)parseXMLFileAtURL: (NSString *)URL{
////    NSLog(@"Start parseXMLFileAtURL");
//    xmlData = [[NSMutableArray alloc] init];
//    NSURL *xmlURL = [NSURL URLWithString:URL];
//    xmlParser = [[NSXMLParser alloc] initWithContentsOfURL:xmlURL];
//    [xmlParser setDelegate:self];
//    [xmlParser parse];
////    NSLog(@"End parseXMLFileAtURL");
//}
// didStartElement
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    NSLog(@"Start didStartElement");
    currentElement = [elementName copy];
    if ([elementName isEqualToString:@"xml"]) {
        item = [[NSMutableDictionary alloc] init];
        currentStatus = [[NSMutableString alloc] init];     
        currentStatus_code = [[NSMutableString alloc]init];
        currentDetail = [[NSMutableString alloc]init];
        currentRefid = [[NSMutableString alloc]init];
        currentDl_link = [[NSMutableString alloc]init];
        currentFilesize = [[NSMutableString alloc]init];
    }
    NSLog(@"End didStartElement");
}
// foundCharacters
- (void)parser:(NSXMLParser *)parser  foundCharacters:(NSString *)string{
    NSLog(@"Start foundCharacters");
    if([currentElement isEqualToString:@"status"]){
        [currentStatus appendString:string];
    }else if([currentElement isEqualToString:@"status_code"]){
        [currentStatus_code appendString:string];
    }else if ([currentElement isEqualToString:@"detail"]) {
        [currentDetail appendString:string];
    }else if([currentElement isEqualToString:@"refid"]){
        [currentRefid appendString:string];
    }else if([currentElement isEqualToString:@"dl_link"]){
        [currentDl_link appendString:string];
    }else if([currentElement isEqualToString:@"filesize"]){
        [currentFilesize appendString:string];
    }
    NSLog(@"End foundCharacters");
}
// didEndElement
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSLog(@"Start didEndElement");
    if ([elementName isEqualToString:@"xml"]) {
        [item setObject:currentStatus forKey:@"status"];
        [item setObject:currentStatus_code forKey:@"status_code"];
        [item setObject:currentDetail forKey:@"detail"];
        [item setObject:currentRefid forKey:@"refid"];
        [item setObject:currentDl_link forKey:@"dl_link"];
        [item setObject:currentFilesize forKey:@"filesize"];
        [xmlData addObject:[item copy]];
        
    }
    NSLog(@"End didEndElement");
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    
}

@end
