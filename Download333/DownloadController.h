//
//  DownloadController.h
//  Download333
//
//  Created by Aphinop Supawaree on 12/27/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DownloadCell.h"
#import "DownloadItem.h"

@interface DownloadController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *downloadList;
    IBOutlet UITableView *downloadTable;
    AppDelegate *appDelegate;
//    MyImage *myImageObj;
}

@property (retain) NSMutableArray *downloadList;
@property (retain) IBOutlet UITableView *downloadTable;

+ (DownloadController *)sharedInstance;

-(int)addDownloadList:(DownloadItem *)download ;

// Update Badge
- (void)UpdateBadge;
- (int)addDownloadList:(DownloadItem *)download;

@end
