//
//  OTP.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/14/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "OTP.h"
#import "AFXMLRequestOperation.h"
@implementation OTP

@synthesize textField, textfieldPhoneNumber;
@synthesize closeBtn, submitBtn;
//@synthesize STATUS, STATUS_CODE, STATUS_DETAIL;
//@synthesize MSISDNRQ;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSLog(@"STATUS = %@", self.STATUS);
        NSLog(@"STATUS_CODE = %@", self.STATUS_CODE);
        NSLog(@"STATUS_DETAIL = %@", self.STATUS_DETAIL);
        NSLog(@"MSISDNRQ = %@", self.MSISDNRQ);
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Text Field Control
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textfieldPhoneNumber.text.length >= 10 && range.length == 0) {
        return NO;
    }else{
        return YES;
    }

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textfieldPhoneNumber resignFirstResponder];
    return YES;
}


#pragma mark - Submit Button Control
- (IBAction)submitBtnTouchUpInside:(id)sender {
    NSString *inputMSISDN = textfieldPhoneNumber.text;
    inputMSISDN = [NSString stringWithFormat:@"66%@",[inputMSISDN substringFromIndex:1]];
    self.MSISDNRQ = inputMSISDN;
    [self connectAPI:inputMSISDN];
}

#pragma mark - Connect API

- (void) connectAPI:(NSString *) msisdnRQ {
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/sendOTP.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@", APPID, appDelegate.deviceId, msisdnRQ, APPVERSION, APIVERSION];
    
    NSURL *pathUrl = [NSURL URLWithString:URL];
//    NSLog(@"url = %@", pathUrl);
//    
//    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:pathUrl];
//    [request setDelegate:self];
//    [request startSynchronous];
//    [request release];

    NSURLRequest *request = [NSURLRequest requestWithURL:pathUrl];
    AFXMLRequestOperation *operation = [AFXMLRequestOperation XMLParserRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSXMLParser *XMLParser) {
        XMLParser.delegate = self;
        [XMLParser parse];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSXMLParser *XMLParse) {
        NSLog(@"error = %@",error);
    }];
    
    [operation start];
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    NSString *respString = [request responseString];
    NSData *responseData = [respString dataUsingEncoding:NSUTF8StringEncoding];    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:responseData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"error = %@",[request error]);
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
//    if (![elementName isEqual:@"XML"]) {
//        tmpData = [[NSMutableString alloc] init]; 
//    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {    
//    [tmpData appendString:string];
    tmpData = string;
    NSLog(@"TEMP DATA %@",tmpData);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {     
    
    if([elementName isEqualToString:@"STATUS"]) {
        self.STATUS = [NSString stringWithFormat:@"%@", tmpData];
    
    } else if([elementName isEqualToString:@"STATUS_CODE"]) {
        self.STATUS_CODE = [NSString stringWithFormat:@"%@", tmpData];

    } else if([elementName isEqualToString:@"STATUS_DETAIL"]) {
        self.STATUS_DETAIL = [NSString stringWithFormat:@"%@", tmpData];
    }
    tmpData = nil;

}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"ENDDDD");
    NSLog(@"STATUS = %@", self.STATUS);
    NSLog(@"STATUS_CODE = %@", self.STATUS_CODE);
    NSLog(@"STATUS_DETAIL = %@", self.STATUS_DETAIL);
    NSLog(@"MSISDNRQ = %@", self.MSISDNRQ);
    
    
    NSLog(@"STATUS =");

    
    if([self.STATUS isEqualToString:@"OK"]){
        appDelegate.MSISDN = self.MSISDNRQ;
        [self.view removeFromSuperview];
        ValidateOTP *validateOTP = [[ValidateOTP alloc] init];
        [validateOTP.view setBackgroundColor:[UIColor clearColor]];
        [appDelegate.window addSubview:validateOTP.view];
    }else{
        [self alertMessageWithTitle:self.STATUS andMessage:self.STATUS_DETAIL];
    }
 
}

#pragma mark - Close Button Control
- (IBAction)btnCloseTouchUpInside:(id)sender {
    [self.view removeFromSuperview];
    [appDelegate exitView];
}


#pragma mark - Alert Message
- (void) alertMessageWithTitle:(NSString*) title andMessage:(NSString*) message {
    UIAlertView *errView;
    
    errView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@", title] 
                                         message:[NSString stringWithFormat:@"%@", message] 
                                        delegate:self 
                               cancelButtonTitle:@"OK" 
                               otherButtonTitles:nil];
        [errView show];
    [errView release];
}

- (void) dealloc {
//    [tmpData release];
//    [STATUS release];
//    [STATUS_CODE release];
//    [STATUS_DETAIL release];
    [super dealloc];
}

@end
