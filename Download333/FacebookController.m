//
//  FacebookController.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/18/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "FacebookController.h"

@implementation FacebookController

@synthesize facebookWebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    // [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString *urlAddress = @"http://www.facebook.com/pages/123and333GMMGrammy/194570580564348"; 
    
    //Create a URL object. 
    NSURL *url = [NSURL URLWithString:urlAddress]; 
    
    //URL Request Object 
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url]; 
    
    [facebookWebView loadRequest:requestObj];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}

- (void)viewDidUnload
{
    [facebookWebView release];
    facebookWebView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [facebookWebView release];
    [super dealloc];
}
@end
