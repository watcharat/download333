//
//  MVPlayer.h
//  Download333
//
//  Created by Aphinop Supawaree on 1/3/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "SongPlayer.h"

@interface MVPlayer : UIViewController
{

    #if __IPHONE_OS_VERSION_MIN_REQUIRED < 30200
        // code for iOS below 3.2
        MPMoviePlayerController *previewMP;
    #else
        // code for iOS 3.2 ++
        MPMoviePlayerViewController *previewMP;
    #endif
    
    NSString *gmmdcode;
    BOOL iosSeven;
    BOOL nowPlaying;
    BOOL portrait;
}

@property (nonatomic,retain) NSString *gmmdcode;

- (NSString *)getSongFilePath:(NSString *)nameOfFile;
@property (retain) MPMoviePlayerController *player;


@end
