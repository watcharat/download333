//
//  SongPlayer.m
//  Download333
//
//  Created by Aphinop Supawaree on 1/2/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "SongPlayer.h"
#import "PlayerPlaylistCell.h"
#import "PlayerPlaylistSongCell.h"

static SongPlayer *sharedSingletonDelegate = nil;
@implementation SongPlayer
@synthesize tableData;
@synthesize songIndex;
@synthesize audioPlayer;
@synthesize playState;
@synthesize curIndex;
@synthesize isLoop;
@synthesize isShuffle;
@synthesize songList;
@synthesize currentPlayingGmmdCode;

NSTimer *timerForUpdateProgress;
PlayerPlaylistCell *playerCell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    NSLog(@"begin didload");
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self.navigationItem setTitle:@"Playlist"];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editNavButtonPressed)] autorelease];
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.tableData setBackgroundColor:[UIColor clearColor]];
    
    [self listSong];
    [self.tableData reloadData];
    
    appDelegate.songPlayingGmmdCode = @"0";
    

    [self searchCurrentIndexWith:self.songList andGMMDCode:appDelegate.songPlayingGmmdCode];

    
    NSLog(@"end didload");
}

- (IBAction)editNavButtonPressed {
    NSLog(@"EDIT PLAYLIST");
    currentPlayingGmmdCode = [[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"gmmdcode"];
//    EditPlaylist *editPlaylist = [[EditPlaylist alloc] init];
//    SongDetailController *nextController = [[SongDetailController alloc] initWithNibName:@"SongDetailView" bundle:[NSBundle mainBundle]];
    EditPlaylist *editPlaylist = [[EditPlaylist alloc] initWithNibName:@"EditPlaylist" bundle:[NSBundle mainBundle]];
    [appDelegate.playlistView pushViewController:editPlaylist animated:YES];
    [editPlaylist release];
}

- (void)viewDidUnload
{
    [self setTableData:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
//    [self listSong];
    self.songList = appDelegate.playlistSongList;
    [self.tableData reloadData];
}

//-(void)viewWillAppear:(BOOL)animated{
//    
//    NSLog(@">>> viewWillAppear");
////    [self becomeFirstResponder];
//    [self listSong];
//    [self.tableData reloadData];
//    
//    if(currentPlayingGmmdCode && ![currentPlayingGmmdCode isEqualToString:@""]){
//        [self searchCurrentIndexWith:self.songList andGMMDCode:currentPlayingGmmdCode];
//    }
//
////    currentPlayingGmmdCode = @"";
//
//    
////    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate]; 
//    //    songsList = [NSArray arrayWithArray:appDelegate.playListSongsNow];
//    
////    NSLog(@"&&&&&&& >>> %@", playerCell);
////    if (playerCell == nil) {
////        NSLog(@"&&&&&&& >>> init");
////        playerCell = [[PlayerPlaylistCell alloc]init];
////    }
//    
//}

- (void)viewDidAppear:(BOOL)animated {
	
    [super viewDidAppear:animated];
	
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(beginReceivingRemoteControlEvents)]) {
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    }
    
    [self becomeFirstResponder];
    
//    if (appDelegate.playState == NO && [appDelegate.playlistSongList count] > 0) {
//        [self playSongs];
//    }
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    NSLog(@"delloc songplayer");
    [tableData release];
//    [audioPlayer release];
    [super dealloc];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    [self searchCurrentIndexWith:self.songList andGMMDCode:appDelegate.songPlayingGmmdCode];
    
    // Return the number of sections.
    return 2;
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([songList count] == 0){
        return nil;
    }
    
    if (section == 0) {
        return 1;
    }
    else
    {
        return [appDelegate.playlistSongList count];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) 
    {
        return 220;
    }
    else
    {
        return 78;
    }    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.section == 0) {
        static NSString *cellIdentifier = @"PlayerPlaylistCellIdentifier";
        
        playerCell = (PlayerPlaylistCell *)[tableData dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(playerCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlayerPlaylistCell" owner:self options:nil];
            playerCell = [nib objectAtIndex:0];
        }
        
        // Configure the cell...
        NSLog(@"cellForRowAtIndexPath SONG INDEX = %d", [songIndex intValue]);
        NSString *songname_th = [[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"songname_th"];
        NSString *songname_en = [[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"songname_en"];
        NSString *songname;
        if ([songname_th isEqualToString:@""]) {
            songname = [NSString stringWithString:songname_en];
        }
        else
        {
            songname = [NSString stringWithString:songname_th];
        }
        
        playerCell.lblSongName.text = songname;
        playerCell.lblArtistName.text = [[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"artist_name"];
        playerCell.lblAlbumName.text = [[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"album_th"];
        
        playerCell.imgThmcover.image = [UIImage imageWithData:[[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"thmcover"]];
        
        return playerCell;
    }
    else
    {
        static NSString *cellIdentifier = @"PlayerPlaylistSongCellIdentifier";
        
        PlayerPlaylistSongCell *Cell = (PlayerPlaylistSongCell *)[tableData dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(Cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlayerPlaylistSongCell" owner:self options:nil];
            Cell = [nib objectAtIndex:0];
        }
        
        // Configure the cell...
        //    [menuButton addTarget:self action:@selector(mainMenu_Clicked_back:) forControlEvents:UIControlEventTouchUpInside];
        
        [Cell.imgBG setAlpha:0.9];
//        if ([self.songIndex intValue] == indexPath.row) {
//            Cell.imgBG.image = [UIImage imageNamed:@"tap_link_on.png"];
//        }
//        else
//        {
//            Cell.imgBG.image = [UIImage imageNamed:@"tap_link_off.png"];
//        }
        
        Cell.imgSong.image = [UIImage imageWithData:[[appDelegate.playlistSongList objectAtIndex:indexPath.row]valueForKey:@"thmcover"]];
        Cell.lblSongName.text = [[appDelegate.playlistSongList objectAtIndex:indexPath.row]valueForKey:@"songname_th"];
        Cell.lblArtistName.text = [[appDelegate.playlistSongList objectAtIndex:indexPath.row]valueForKey:@"artist_name"];
        return Cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1) {
        NSLog(@"indexPath.row = %d", indexPath.row);
        curIndex = indexPath.row;
        self.songIndex = [NSString stringWithFormat:@"%d", curIndex];

        [self PlayWhenYesPlayState];
        [self.tableData reloadData];
        [self.tableData setContentOffset:CGPointZero animated:YES];
    }
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        return NO;
    }
    return YES;
}

//-(void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
////        [self.tableData deleteRowAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        NSLog(@"indexPath.section = %d", indexPath.section);
//    }
//}


#pragma mark SharedInstance
+ (SongPlayer *)sharedInstance
{
	@synchronized(self) {
		if (sharedSingletonDelegate == nil) {
			sharedSingletonDelegate = [[self alloc]init]; 
            //            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
		}
	}
	return sharedSingletonDelegate;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (sharedSingletonDelegate == nil) {
			sharedSingletonDelegate = [super allocWithZone:zone];
			return sharedSingletonDelegate;  
		}
	}
	return nil; 
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#pragma mark - song path
- (NSString *)getSongFilePath:(NSString *)nameOfFile
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *filename = [[NSString alloc]initWithFormat:@"%@.mp3",nameOfFile];
    NSString *uniquePath = [documentDirectory stringByAppendingPathComponent:filename];
    
    return uniquePath;
    
}

#pragma mark - Remote control event handle
- (void) remoteControlReceivedWithEvent:(UIEvent *)event
{
    
    if (event.type == UIEventTypeRemoteControl) {
        
        switch (event.subtype) {
                
            case UIEventSubtypeRemoteControlTogglePlayPause:
                
                NSLog(@"+++++++++++++++++++++ PlayPause");
//                [self playSongs];
                
                break;
                
            case UIEventSubtypeRemoteControlNextTrack:
                
                NSLog(@"+++++++++++++++++++++ NextSong");
//                [self NextSongs];
                
                break;
                
            case UIEventSubtypeRemoteControlPreviousTrack:
                
                NSLog(@"+++++++++++++++++++++ PreviousSong");
//                [self PrevSongs];
                
                break;
                
            default:
                break;
        }
        
    }
    
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

#pragma mark - ButtonAction
- (IBAction)PlayPause:(id)sender
{
    [self playSongs];
}

- (IBAction)nextSong:(id)sender 
{
    [self nextSongs];
}

- (IBAction)prevSong:(id)sender 
{
    [self prevSongs];
}

- (IBAction)shuffleSong:(id)sender 
{
    [self setShuffle];
}

- (IBAction)repeatSong:(id)sender 
{
    [self setRepeat];
}

- (IBAction)updateTracker:(id)sender 
{
    [audioPlayer pause];
	audioPlayer.currentTime = playerCell.trackerSong.value * audioPlayer.duration;
	//[audioPlayer playAtTime:trackBar.value * audioPlayer.duration];
	[audioPlayer play];
}

#pragma mark - Player
- (void)playSongs
{
    NSLog(@">>> playSongs at Index : %@", self.songIndex);
    if (playState == NO && [appDelegate.playlistSongList count] > 0) {
        NSLog(@"playState == NO");
        
        appDelegate.playState = YES;
        playState = appDelegate.playState;
        if (audioPlayer == nil) {
            
            NSString *song_path = [self getSongFilePath:[[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"gmmdcode"]];
            appDelegate.songPlayingGmmdCode = [[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"gmmdcode"];
            
            if (audioPlayer != nil) [audioPlayer release]; 
            
            // Load the array with the sample file
            NSURL *fileURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@", song_path]];
            NSLog(@"%@", fileURL);
            
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
            audioPlayer.numberOfLoops = -1;
            [audioPlayer setDelegate:self];
            
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
            [[AVAudioSession sharedInstance] setActive:YES error:nil];
            
            [audioPlayer prepareToPlay];
            [audioPlayer setNumberOfLoops:0];
            
            if (audioPlayer == nil)
                NSLog([error description]);
            else
                [audioPlayer play];
            timerForUpdateProgress	= [NSTimer scheduledTimerWithTimeInterval:(1.0) target:self selector:@selector(updater) userInfo:nil repeats:YES];
    
        }
        else 
        {
            [audioPlayer play];
        }
        
        [playerCell.btnPlay setImage:[UIImage imageNamed:@"icon_pause_on.png"] forState:UIControlStateNormal];
        
    }
    else if (playState == YES) 
    {
        NSLog(@"playState == YES");
        [self pauseSong];
    }       
         
}

- (void)PlayWhenYesPlayState
{

    appDelegate.playState = NO;
    playState = appDelegate.playState;
    [audioPlayer stop];
    audioPlayer = nil;
    [playerCell.btnPlay setImage:[UIImage imageNamed:@"icon_pause_on.png"] forState:UIControlStateNormal];
    [self playSongs];       
    
}

- (void)pauseSong
{
    appDelegate.playState = NO;
	playState = NO;
	[audioPlayer pause];
	[playerCell.btnPlay setImage:[UIImage imageNamed:@"icon_play_off.png"] forState:UIControlStateNormal];	
}

-(void)nextSongs
{
    NSLog(@"nextSongs");
    
    appDelegate.playState = NO;
    playState = NO;
    [audioPlayer stop];
    audioPlayer = nil;
    [playerCell.btnPlay setImage:[UIImage imageNamed:@"icon_play_on.png"] forState:UIControlStateNormal];
    
    curIndex = [self.songIndex intValue];
    
    if (isLoop) {
               
        if (([appDelegate.playlistSongList count] > 0) && (curIndex < ([appDelegate.playlistSongList count]-1))) 
        {
            curIndex = curIndex+1;
//            NSLog(@"curIndex : %d", curIndex);
            self.songIndex = [NSString stringWithFormat:@"%d", curIndex];
//            NSLog(@"self.songIndex : %@", self.songIndex);
        }
        else{
            self.songIndex = @"0";
        }
        [self playSongs];
         
    }
    else
    {
        
        if (([appDelegate.playlistSongList count] > 0) && (curIndex < ([appDelegate.playlistSongList count]-1))) 
        {
            curIndex = curIndex+1;
//            NSLog(@"curIndex : %d", curIndex);
            self.songIndex = [NSString stringWithFormat:@"%d", curIndex];
//            NSLog(@"self.songIndex : %@", self.songIndex);
            [self playSongs];
        }
        else{
            [playerCell.btnPlay setImage:[UIImage imageNamed:@"icon_play_off.png"] forState:UIControlStateNormal];
        }        
        
    }
    
    [self.tableData reloadData];  
    
}

-(void)prevSongs
{
    NSLog(@"prevSongs");
    
    if (isLoop) {
        
        appDelegate.playState = NO;
        playState = NO;
        [audioPlayer stop];
        audioPlayer = nil;
        [playerCell.btnPlay setImage:[UIImage imageNamed:@"icon_play_on.png"] forState:UIControlStateNormal];
        
        curIndex = [self.songIndex intValue];
        
        if (([appDelegate.playlistSongList count] > 0) && (curIndex == 0)) {
            curIndex = [appDelegate.playlistSongList count]-1;
            self.songIndex = [NSString stringWithFormat:@"%d", curIndex];
        }
        else if (([appDelegate.playlistSongList count] > 0) && curIndex != 0){
            curIndex = curIndex-1;
            self.songIndex = [NSString stringWithFormat:@"%d", curIndex];
        }
        [self playSongs];
        
    }
    else
    {
        if (([appDelegate.playlistSongList count] > 0) && curIndex != 0){
            
            appDelegate.playState = NO;
            playState = NO;
            [audioPlayer stop];
            audioPlayer = nil;
            [playerCell.btnPlay setImage:[UIImage imageNamed:@"icon_play_on.png"] forState:UIControlStateNormal];
            
            curIndex = [self.songIndex intValue];
            
            curIndex = curIndex-1;
            self.songIndex = [NSString stringWithFormat:@"%d", curIndex];
            [self playSongs];
        }
        
    }
    
    [self.tableData reloadData];
}

-(void)updater {
    
    //    float songTime = (audioPlayer.currentTime/audioPlayer.duration);
    //	seektrack.progress = songTime;
    //    
    //
    //    lblCurrentTime.text = [NSString stringWithFormat:@"%.2f",self.audioPlayer.currentTime/60];
    
    if (audioPlayer != nil) {
        playerCell.trackerSong.value = (audioPlayer.currentTime/audioPlayer.duration);
		//NSLog(@"value=%f",trackBar.value);
        playerCell.playerLbltime1.text = [NSString stringWithFormat:@"%.2f",audioPlayer.currentTime/60];
        playerCell.playerLbltime2.text = [NSString stringWithFormat:@"-%.2f",audioPlayer.duration/60-audioPlayer.currentTime/60];
	}
    
}

- (void)setRepeat
{
    if (isLoop == YES) {
        NSLog(@"loopSongs NO");
        isLoop = NO;
        [playerCell.btnRepeat setImage:[UIImage imageNamed:@"icon_repeat_off.png"] forState:UIControlStateNormal];
    }else{
        NSLog(@"loopSongs YES");
        isLoop = YES;
        [playerCell.btnRepeat setImage:[UIImage imageNamed:@"icon_repeat_on.png"] forState:UIControlStateNormal];
    }
}

- (void)setShuffle
{
    if (isShuffle) {
        NSLog(@"isShuffle YES new isShuffle NO");
        isShuffle = NO;
        appDelegate.playlistSongList = [NSMutableArray arrayWithArray:appDelegate.playlistSongDefault];
//        [self searchCurrentIndexWith:appDelegate.playlistSongList anGMMDCode:[[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"gmmdcode"]];
        [playerCell.btnShuffle setImage:[UIImage imageNamed:@"icon_shuffle_off.png"] forState:UIControlStateNormal];
    }else{
        
        NSLog(@"isShuffle NO  new isShuffle YES");
        isShuffle = YES;
        [self shuffle];
        [self searchCurrentIndexWith:appDelegate.playlistSongList andGMMDCode:[[appDelegate.playlistSongList objectAtIndex:[songIndex intValue]] valueForKey:@"gmmdcode"]];
        [playerCell.btnShuffle setImage:[UIImage imageNamed:@"icon_shuffle_on.png"] forState:UIControlStateNormal];
        [self playSongs];
        
    }
    self.songList = appDelegate.playlistSongList;
    [self.tableData reloadData];
}

- (void)shuffle
{
    
    appDelegate.playlistSongDefault = [NSMutableArray arrayWithArray:appDelegate.playlistSongList];
    appDelegate.playlistSongShuffle = [NSMutableArray arrayWithArray:appDelegate.playlistSongList];
    srandom(time(NULL));
    NSUInteger count = [appDelegate.playlistSongShuffle count];
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        int nElements = count - i;
        int n = (random() % nElements) + i;
        [appDelegate.playlistSongShuffle exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    
    appDelegate.playlistSongList = [NSArray arrayWithArray:appDelegate.playlistSongShuffle];
    
    NSLog(@"appDelegate.playListSongsNow : %@",[appDelegate.playlistSongList valueForKey:@"gmmdcode"]);
    NSLog(@"appDelegate.playlistSongForShuffle : %@",[appDelegate.playlistSongShuffle valueForKey:@"gmmdcode"]);
    NSLog(@"appDelegate.playlistSongDefault : %@",[appDelegate.playlistSongDefault valueForKey:@"gmmdcode"]);
    
}

- (void)searchCurrentIndexWith:(NSArray *)array andGMMDCode:(NSString *)_gmmdcode
{

    
    int countArray = 0;
    curIndex = [self.songIndex intValue]; 
    for (NSArray *items in array) {
        if ([[items valueForKey:@"gmmdcode"] isEqualToString:_gmmdcode]) {
            
            curIndex = countArray;
            self.songIndex = [NSString stringWithFormat:@"%d", curIndex];
            
        }
        
        countArray++;
    }
    
}

#pragma mark AVAudioPlayer delegate Method

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag	
{
    NSLog(@"audioPlayerDidFinishPlaying");
	if (player == audioPlayer)
	{
		if (flag == NO)
        {
//            isPlayFinish = NO;
            NSLog(@"Playback finished unsuccessfully");
        }
		else 
		{
//            isPlayFinish = YES;
            [self nextSongs];
            
		}
        
	}
	
}


- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
	NSLog(@"ERROR IN DECODE : %@\n",error);
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
	NSLog(@"beginINTERRUPT");
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
	NSLog(@"endINTERRUPT");
}


#pragma - mark ManagedObject
- (void)listSong
{
	
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    
    [fetchRequest setResultType:NSDictionaryResultType];
    
    [fetchRequest setEntity:entity];
    
    NSString *predicateCondition = [NSString stringWithFormat:@"service_id = 1 AND inPlaylist = 'YES'"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
    
    
    NSError *error = nil;
    NSArray *results = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    [fetchRequest release];
    
    if ([results count]) {
        
        self.songList = [NSArray arrayWithArray:results];
    }
    else
    {
        self.songList = nil;
    }
    
    appDelegate.playlistSongList = self.songList;

}


- (void)changeBtnPlayer
{
    [playerCell.btnPlay setImage:[UIImage imageNamed:@"icon_play_off.png"] forState:UIControlStateNormal];
    appDelegate.songPlayingGmmdCode = @"";
    self.songIndex = @"0";
}

- (void) resetAfterEdit {
//    currentPlayingGmmdCode = @"";
//    self.songIndex = @"0";
    self.songIndex = @"0";
}


@end
