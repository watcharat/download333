//
//  ShelfImageView.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "ShelfImageView.h"

@implementation ShelfImageView

-(void)loadImageFromURL:(NSString*)_url {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	self.contentMode = UIViewContentModeScaleAspectFit;
	
	[pool release];
	
}

#pragma mark ImageCache
- (void)thumbnailImage:(NSString*)fileName
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	self.contentMode = UIViewContentModeScaleAspectFit;
	AppDelegate *appDelegate =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
	
    if(!appDelegate.thumbnailCache){
        appDelegate.thumbnailCache = [[NSMutableDictionary alloc]init];
    }
    
	UIImage *thumbnail = [appDelegate.thumbnailCache objectForKey:fileName];
	if (nil == thumbnail)
	{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSURL *url = [[NSURL alloc] initWithString:fileName];
		NSError *error;
		if (url != nil) {
//            NSLog(@"START");
//            self.image = [UIImage imageNamed:@"app-icon57x57.png"];
            
            
            
			NSData *data = [[NSData alloc] initWithContentsOfURL:url options:2 error:&error];
//			NSLog(@"END");
			thumbnail = [UIImage imageWithData:data];
			if (thumbnail != nil)
			{	
                [appDelegate.thumbnailCache setObject:thumbnail forKey:fileName];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            }
			[data release];
		}
		[url release];
        
	}
    
	if ( thumbnail != nil) self.image = thumbnail;
	appDelegate = nil;
	[pool release];
}

-(void)dealloc {
	
	[super dealloc];
}

@end
