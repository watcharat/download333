//
//  XMLDownload.h
//  ArtistApp
//
//  Created by  on 9/7/54 BE.
//  Copyright 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface XMLDownload : NSObject {
    
    NSXMLParser *xmlParser;
    NSMutableArray *xmlData;
    NSMutableDictionary *item;
    NSString *currentElement;  
    
    NSMutableString *currentStatus;
    NSMutableString *currentStatus_code;    
    NSMutableString *currentDetail; 
    NSMutableString *currentRefid;
    NSMutableString *currentDl_link;
    NSMutableString *currentFilesize;
    
    BOOL drRecord;
    
    AppDelegate *appDelegate;
}

@property (nonatomic, readonly) NSMutableArray *xmlData;
@property (nonatomic, readonly) BOOL drRecord;

-(id)initWithData:(NSData *)dataXml;

- (NSString *)getStatusCode;
- (void)checkDLServiceByStatusCode:(NSString *)statusCode andMsisdn:(NSString *)msisdn andAlbumId:(NSString *)albumId;

@end
