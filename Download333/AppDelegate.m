//
//  AppDelegate.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/9/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "MainView.h"

@implementation AppDelegate

@synthesize window = _window;

// Reachability
@synthesize remoteHostStatus;
@synthesize internetConnectionStatus;
@synthesize localWiFiConnectionStatus;

// Core Data
@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize MSISDN, OPERATOR;
@synthesize _n;

@synthesize mainTab;
@synthesize playListTab;

@synthesize mainViewController;
@synthesize playlistView;

@synthesize thumbnailCache;

@synthesize playlistSongList;
@synthesize playlistSongDefault;
@synthesize playlistSongShuffle;
@synthesize playState;

@synthesize contentList;

@synthesize songPlayingGmmdCode;
@synthesize deviceId;


// UIAlertView
UITextField *txtserial;

- (void)dealloc
{
    [_window release];
    [_managedObjectContext release];
    [_managedObjectModel release];
    [_persistentStoreCoordinator release];
    [mainTab release];
    [mainViewController release];
    [thumbnailCache release];
    [playlistSongList release];
    [playlistSongDefault release];
    [playlistSongShuffle release];
    [contentList release];
    [playListTab release];
    [playlistView release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.deviceId = [self idForDevice];
    
    NSString *osversion = [[UIDevice currentDevice] systemVersion];
    NSLog(@"osversion = %@",osversion);
    if ([[osversion substringWithRange:NSMakeRange(0,1)] caseInsensitiveCompare:@"7"] == NSOrderedSame) {
        [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setTintColor:[UIColor lightGrayColor]];
        
    }
    
    if([self detectInternetConnection]){   
        NSLog(@"Internet Connection = YES");
        internetConnection = YES;
        wifiConnection = NO;
        
        if([self detectWifiConnection]){
            wifiConnection = YES;
            NSLog(@"Wifi Connection = YES");
        }
        
        AppleMSISDN *appleMSISDN = [[AppleMSISDN alloc] init];
        [appleMSISDN getAppleMSISDN];
        [appleMSISDN release];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"PROFILE" inManagedObjectContext:self.managedObjectContext];
        
        NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
        [request setEntity:entity];
        NSArray *profiles = [self.managedObjectContext executeFetchRequest:request error:nil];
                
        for(PROFILE *profile in profiles) {
            MSISDN = profile.msisdn;
        }
        
        
// TODO :: fixed to test
//        MSISDN = @"66850646820";
        //MSISDN = @"66888096330";
        
        //MSISDN = @"66814379955";
        
        NSLog(@"MSISDN = %@", MSISDN);
        self._n = MSISDN;
        if(MSISDN.length > 11){
            MSISDN = @"";
            [self saveProfileWithMSISDN:MSISDN andCreateDate:[NSDate date]];
        }
        
        if(MSISDN == nil || [MSISDN isEqualToString:@""]){
            NSLog(@"MSISDN IS EMPTY");
            MSISDN = @"";
            GetMSISDN *getMSISDN = [[GetMSISDN alloc] init];
            [getMSISDN getMSISDN];

            if([MSISDN isEqualToString:@""]){
                OTP *otp = [[OTP alloc] init];
                [otp.view setBackgroundColor:[UIColor clearColor]];
                [_window addSubview:otp.view];
            }else{
                CheckMember *checkMember = [[CheckMember alloc] init];
                [_window addSubview:checkMember.view];
            }
        }else{
            self.window.rootViewController = self.mainTab;
        }
        
    }else{
        NSLog(@"Internet Connection = NO");
        internetConnection = NO;
        wifiConnection = NO;
        self.window.rootViewController = self.playListTab;
    }
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (NSString *) idForDevice;
{
    NSString *result = @"";
    
    UIDevice *thisDevice = [UIDevice currentDevice];
    if ([thisDevice respondsToSelector: @selector(identifierForVendor)])
    {
        NSUUID *myID = [[UIDevice currentDevice] identifierForVendor];
        result = [myID UUIDString];
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        result = [defaults objectForKey: @"appID"];
        if (!result)
        {
            CFUUIDRef myCFUUID = CFUUIDCreate(kCFAllocatorDefault);
            result = (NSString *) CFUUIDCreateString(kCFAllocatorDefault, myCFUUID);
            [defaults setObject: result forKey: @"appID"];
            [defaults synchronize];
            CFRelease(myCFUUID);
        }
    }
    //NSLog(@"UDID = %@",result);
    return result;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
    [self saveContext];
}

- (BOOL)detectInternetConnection {
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
	
	hostReach = [[Reachability reachabilityWithHostName: @"www.google.com"] retain];
	[hostReach startNotifier];
    
    internetReach = [[Reachability reachabilityForInternetConnection] retain];
	[internetReach startNotifier];
	
    wifiReach = [[Reachability reachabilityForLocalWiFi] retain];
	[wifiReach startNotifier];
	
	self.remoteHostStatus = [hostReach currentReachabilityStatus];
	self.internetConnectionStatus = [internetReach currentReachabilityStatus];
	self.localWiFiConnectionStatus = [wifiReach currentReachabilityStatus];
	
    if(self.internetConnectionStatus == NotReachable){
        return NO;
    }else{
        return YES;
    }
}

- (BOOL)detectWifiConnection {
    if (self.localWiFiConnectionStatus == ReachableViaWiFi) {
		return YES;
	}
    return NO;
}

- (void) custonAlert : (NSString *)title andMassage:(NSString *)_msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:_msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
    [alert show];
    [alert release];
}

- (NSURL *) documentsDirectory {
    return [[[NSFileManager defaultManager]URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask]lastObject];
}

- (NSManagedObjectModel *) managedObjectModel {
    if(_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Download333" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *) persistentStoreCoordinator {
    if(_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self documentsDirectory] URLByAppendingPathComponent: @"Download333.sqlite"];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Error connecting persistent store: %@, %@", error, [error userInfo]);
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *) managedObjectContext {
    if(_managedObjectContext != nil){
        return _managedObjectContext;
    }
    
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    return _managedObjectContext;
}

- (void) saveContext {
    NSError *error = nil;
    if([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error]){
        NSLog(@"Error saving context: %@, %@", error, [error userInfo]);
        abort();
    }
}


#pragma mark - Save Profile Data
- (void) saveProfileWithMSISDN:(NSString *) msisdn andCreateDate:(NSDate *) createDate {
    PROFILE *profile = [NSEntityDescription insertNewObjectForEntityForName:@"PROFILE" inManagedObjectContext:self.managedObjectContext];
    profile.msisdn = msisdn;
    profile.create_date = createDate;
    
    NSError* error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save to data store: %@", [error localizedDescription]);
        NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
        if(detailedErrors != nil && [detailedErrors count] > 0) {
            for(NSError* detailedError in detailedErrors) {
                NSLog(@"  DetailedError: %@", [detailedError userInfo]);
            }
        }
        else {
            NSLog(@"  %@", [error userInfo]);
        }
    }else{
        NSLog(@"SAVE DATA SUCCESS");
    }
}


#pragma mark - Exit View
- (void) exitView {
    Exit *exit = [[Exit alloc] init];
    [exit.view setBackgroundColor:[UIColor clearColor]];
    [_window addSubview:exit.view];
}

@end
