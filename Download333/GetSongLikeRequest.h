//
//  GetSongLikeRequest.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/4/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface GetSongLikeRequest : UITableViewCell {
//    NSString *tmpData;
    NSMutableString *tmpData;
    NSString *status;
    NSString *statusCode;
    NSString *statusDetail;
//    NSString *gmmdCode;
    NSString *songLikeCount;
}

@property (retain, nonatomic) NSString *status;
@property (retain, nonatomic) NSString *statusCode;
@property (retain, nonatomic) NSString *statusDetail;
//@property (retain, nonatomic) NSString *gmmdCode;
@property (retain, nonatomic) NSString *songLikeCount;

@end
