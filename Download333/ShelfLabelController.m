//
//  ShelfLabelController.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "ShelfLabelController.h"
#import "AFXMLRequestOperation.h"
@implementation ShelfLabelController

@synthesize ID;
@synthesize NAMETH;
@synthesize NAMEEN;
@synthesize DESC;
@synthesize TYPE;
@synthesize LEVEL;
@synthesize SHELF_ID;
@synthesize SHELF_ICON;
@synthesize SHELF_MENU_IMAGE_ON;
@synthesize SHELF_MENU_IMAGE_OFF;
@synthesize tableView;
@synthesize searchString;

@synthesize songItem;
@synthesize controller;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    self.tableView.backgroundColor = [UIColor clearColor];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if(!controller){
        controller = [[NSMutableArray alloc] init];
    }
    
    self.navigationItem.title = NAMEEN;
    
    // ---- Setting Navigate Left Button
    BackNavigateButton *btnBackView = [[BackNavigateButton alloc] init];
    [btnBackView addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithCustomView:btnBackView];
    self.navigationItem.leftBarButtonItem = btnBack;
    // ---- End Setting Navigate Left Button
    
//    [self.navigationBar pushNavigationItem:self.navigationItem animated:NO];
    
    [self connectAPI];
}

- (void)Back:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES]; 
    if([[self.navigationController childViewControllers] count] >0 ){
        [self.navigationController popViewControllerAnimated:YES]; 
    }
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [tableView release];
    [super dealloc];
}


#pragma mark - Table View Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [controller count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *SONG_CELL_IDENTIFIER = @"SongCellIdentifier";
    SongCell *songCell = (SongCell *) [tableView dequeueReusableCellWithIdentifier:SONG_CELL_IDENTIFIER];
    
    if(songCell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SongCell" owner:self options:nil];
        songCell = [nib objectAtIndex:0];
    }
    
    songCell.songName.text = [(SongDetailController *)[controller objectAtIndex:indexPath.row] SongNameTH];
    songCell.artistName.text = [(SongDetailController *)[controller objectAtIndex:indexPath.row] ArtistTH];
    songCell.backgroundColor = [UIColor clearColor];
//    UIImage *_image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[(SongDetailController *)[controller objectAtIndex:indexPath.row] thmCover]]]];
//    
//    songCell.thumbImage.image = _image;
//    
//    [_image release];
    
    CGRect frame;
    frame.size.width = 50;
    frame.size.height = 50;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [[songCell.contentView viewWithTag:1] removeFromSuperview];
    ShelfImageView *myImage = [[ShelfImageView alloc] initWithFrame:frame];
    myImage.tag = 1;
    [NSThread detachNewThreadSelector:@selector(thumbnailImage:) 
                             toTarget:myImage 
                           withObject:[NSString stringWithFormat:@"%@", [(SongDetailController *)[controller objectAtIndex:indexPath.row] thmCover]]];
    [songCell.thumbImage addSubview:myImage];
    [myImage release];
    
    return  songCell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 76.0;
}


#pragma mark - Table View Selected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSUInteger row = [indexPath row];
    NSLog(@"ROW = %d", row);
    
    SongDetailController *nextController = [[SongDetailController alloc] initWithNibName:@"SongDetailView" bundle:[NSBundle mainBundle]];
    
    nextController.Content = [[controller objectAtIndex:row] valueForKey:@"Content"];
    nextController.ContentCode = [[controller objectAtIndex:row] valueForKey:@"ContentCode"];
    nextController.GMMDCode = [[controller objectAtIndex:row] valueForKey:@"GMMDCode"];
    nextController.sVide = [[controller objectAtIndex:row] valueForKey:@"sVide"];
    nextController.SongNameEN = [[controller objectAtIndex:row] valueForKey:@"SongNameEN"];
    nextController.SongNameTH = [[controller objectAtIndex:row] valueForKey:@"SongNameTH"];
    nextController.thmCover = [[controller objectAtIndex:row] valueForKey:@"thmCover"];
    nextController.cover = [[controller objectAtIndex:row] valueForKey:@"cover"];
    nextController.AlbumTH = [[controller objectAtIndex:row] valueForKey:@"AlbumTH"];
    nextController.ArtistEN = [[controller objectAtIndex:row] valueForKey:@"ArtistEN"];
    nextController.ArtistTH = [[controller objectAtIndex:row] valueForKey:@"ArtistTH"];
    nextController.preview = [[controller objectAtIndex:row] valueForKey:@"preview"];
    nextController.sDuration = [[controller objectAtIndex:row] valueForKey:@"sDuration"];
    nextController.rbt = [[controller objectAtIndex:row] valueForKey:@"rbt"];
    nextController.CP_ID = [[controller objectAtIndex:row] valueForKey:@"CP_ID"];
    nextController.mvCover = [[controller objectAtIndex:row] valueForKey:@"mvCover"];
    nextController.streaming_mv = [[controller objectAtIndex:row] valueForKey:@"streaming_mv"];
    nextController.streaming_fs = [[controller objectAtIndex:row] valueForKey:@"streaming_fs"];
    nextController.lyrics = [[controller objectAtIndex:row] valueForKey:@"lyrics"];
    
    [appDelegate.mainViewController pushViewController:nextController animated:YES];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [nextController release];
    
}


#pragma mark - Connect API
- (void) connectAPI {
    
    NSString *URL;
    NSLog(@"SHELF_ID = %@", SHELF_ID);
    if([SHELF_ID isEqualToString:@""]){
        URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/search.jsp?APP_ID=%@&DEVICE=%@&KEY=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@", APPID, appDelegate.deviceId, searchString, appDelegate.MSISDN, APPVERSION, APIVERSION];
    }else{
        URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/songListInShelf.jsp?APP_ID=%@&DEVICE=%@&SHELF_ID=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@", APPID, appDelegate.deviceId, SHELF_ID, appDelegate.MSISDN, APPVERSION, APIVERSION];
    }
    
    NSURL *pathUrl = [NSURL URLWithString:[URL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
//    NSLog(@"url = %@", pathUrl);
//    
//    request = [ASIHTTPRequest requestWithURL:pathUrl];
//    [request setDelegate:self];
//    [request startSynchronous];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:pathUrl];
    AFXMLRequestOperation *operation = [AFXMLRequestOperation XMLParserRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSXMLParser *XMLParser) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        shelfArray = [[NSMutableArray alloc] init];
        XMLParser.delegate = self;
        [XMLParser parse];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSXMLParser *XMLParse) {
        NSLog(@"error = %@",error);
    }];
    [operation start];
}

- (void)requestStarted:(ASIHTTPRequest *)request {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSLog(@"error = %@",[request error]);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [appDelegate custonAlert:@"Error" andMassage:@"Connection time out. Please try again."];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    shelfArray = [[NSMutableArray alloc] init];
    NSString *respString = [request responseString];
    NSData *respData = [respString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData: respData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if(qName) {
        elementName = qName;
    }
    
    if (appDelegate.contentList == nil) appDelegate.contentList = [[NSMutableArray alloc] init];
    
    if([elementName isEqualToString:@"Content"]) {
        self.songItem = [[SongItem alloc] init];
    }else{
        if(!tmpData){
            tmpData = [[NSMutableString alloc] init];
        }
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {    
    //    tmpData = string;
    if (tmpData) {
        [tmpData appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if(qName) {
        elementName = qName;
    }
    
    if([elementName isEqualToString:@"Content"]) {
        [appDelegate.contentList addObject:songItem];
//        NSLog(@"contentList = %@", appDelegate.contentList);
        [shelfArray addObject:songItem];
        [songItem release];
        songItem = nil;
    } else if([elementName isEqualToString:@"ContentCode"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"GMMDCode"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"sVide"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"SongNameEN"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"SongNameTH"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"thmCover"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"cover"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"AlbumTH"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"ArtistEN"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"ArtistTH"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"preview"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"sDuration"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"rbt"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"CP_ID"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"mvCover"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"streaming_mv"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"streaming_fs"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else if([elementName isEqualToString:@"lyrics"]) {
        [songItem setValue:[tmpData stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:elementName];
        [tmpData release];
        tmpData = nil;
    } else {
        [tmpData release];
        tmpData = nil;
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    SongDetailController *songDetailController;
        
    for (SongItem *item in shelfArray) {
        songDetailController = [[SongDetailController alloc] init];
        songDetailController.ContentCode = item.ContentCode;
        songDetailController.GMMDCode = item.GMMDCode;
        songDetailController.sVide = item.sVide;
        songDetailController.SongNameEN = item.SongNameEN;
        songDetailController.SongNameTH = item.SongNameTH;
        songDetailController.thmCover = item.thmCover;
        songDetailController.cover = item.cover;
        songDetailController.AlbumTH = item.AlbumTH;
        songDetailController.ArtistEN = item.ArtistEN;
        songDetailController.ArtistTH = item.ArtistTH;
        songDetailController.preview = item.preview;
        songDetailController.sDuration = item.sDuration;
        songDetailController.rbt = item.rbt;
        songDetailController.CP_ID = item.CP_ID;
        songDetailController.mvCover = item.mvCover;
        songDetailController.streaming_mv = item.streaming_mv;
        songDetailController.streaming_fs = item.streaming_fs;
        songDetailController.lyrics = item.lyrics;
        
//        NSLog(@"thmCover = %@", item.thmCover);
        
        [self.controller addObject:songDetailController];
        
        [songDetailController release];
        songDetailController = nil;
    }
    
    [self.tableView setNeedsDisplay];
    [self.tableView setNeedsLayout];
    [self.tableView reloadData];
    
}



@end





