//
//  SongDetailController.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Twitter/Twitter.h>
#import "AppDelegate.h"
#import "SongDetailCell.h"
#import "ShelfImageView.h"
#import "DownloadItem.h"
#import "DownloadController.h"
#import "SongDetailRelateCell.h"
#import "SongItem.h"
#import "GetSongLikeRequest.h"
#import "FBConnect.h"

#if defined(__IPHONE_5_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_5_0
#import <Twitter/Twitter.h>
#endif



@interface SongDetailController : UIViewController <UITableViewDataSource, UITableViewDelegate, AVAudioPlayerDelegate, UIActionSheetDelegate, ASIHTTPRequestDelegate, UIApplicationDelegate, FBSessionDelegate> {
    
    AppDelegate *appDelegate;
    ASIHTTPRequest *request;
    
    NSString *Content;
    NSString *ContentCode;
    NSString *GMMDCode;
    NSString *sVide;
    NSString *SongNameEN;
    NSString *SongNameTH;
    NSString *thmCover;
    NSString *cover;
    NSString *AlbumTH;
    NSString *ArtistEN;
    NSString *ArtistTH;
    NSString *preview;
    NSString *sDuration;
    NSString *rbt;
    NSString *CP_ID;
    NSString *mvCover;
    NSString *streaming_mv;
    NSString *streaming_fs;
    NSString *lyrics;
    
        
    NSMutableArray *xmlData;
    
    IBOutlet UITableView *tableView;
    
    AVPlayer *player;
    BOOL playing;
    
    NSTimer *timer;
    
    BOOL boolLikeStatus;
    
//    NSString *likeStatus
    NSString *likeStatus;
    NSString *likeStatusCode;
    NSString *likeStatusDetail;
    NSString *likeCount;
    
    id btnPlay;
    
    
    Facebook *facebook;
    BOOL _canTweet;
    
}

@property (nonatomic, retain) NSString *Content;
@property (nonatomic, retain) NSString *ContentCode;
@property (nonatomic, retain) NSString *GMMDCode;
@property (nonatomic, retain) NSString *sVide;
@property (nonatomic, retain) NSString *SongNameEN;
@property (nonatomic, retain) NSString *SongNameTH;
@property (nonatomic, retain) NSString *thmCover;
@property (nonatomic, retain) NSString *cover;
@property (nonatomic, retain) NSString *AlbumTH;
@property (nonatomic, retain) NSString *ArtistEN;
@property (nonatomic, retain) NSString *ArtistTH;
@property (nonatomic, retain) NSString *preview;
@property (nonatomic, retain) NSString *sDuration;
@property (nonatomic, retain) NSString *rbt;
@property (nonatomic, retain) NSString *CP_ID;
@property (nonatomic, retain) NSString *mvCover;
@property (nonatomic, retain) NSString *streaming_mv;
@property (nonatomic, retain) NSString *streaming_fs;
@property (nonatomic, retain) NSString *lyrics;

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) AVPlayer *player;
@property (nonatomic, retain) NSTimer *timer;


@property (nonatomic, retain) Facebook *facebook;


- (IBAction)playPreview:(id)sender;

-(void) completePreviewTime;

- (IBAction)downloadActionSheet:(id)sender;
- (void)totabDownload;
- (void)addSongToDownloadWithURL:(NSString *)_url andService:(NSString *)_SERVICE_ID;
- (void)postDownloadURLInBackgroundWithType:(NSString *)_SERVICE_ID;

- (BOOL)checkLibraryWithGMMDCode:(NSString *)_gmmdCode andServiceId:(NSString *)_service_id;

- (NSArray *) getSongLike:(NSString *)_gmmdCode;
- (void) getLikeInfo;

@end
