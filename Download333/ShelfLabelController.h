//
//  ShelfLabelController.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ShelfLabelCell.h"
#import "ASIHTTPRequest.h"
#import "SongItem.h"
#import "SongDetailController.h"
#import "SongCell.h"
#import "ShelfImageView.h"
#import "BackNavigateButton.h"

@interface ShelfLabelController : UIViewController <UITableViewDataSource, UITableViewDelegate,NSXMLParserDelegate>{
    AppDelegate *appDelegate;
    ASIHTTPRequest *request;
    NSString *ID;
    NSString *NAMETH;
    NSString *NAMEEN;
    NSString *DESC;
    NSString *TYPE;
    NSString *LEVEL;
    NSString *SHELF_ID;
    NSString *SHELF_ICON;
    NSString *SHELF_MENU_IMAGE_ON;
    NSString *SHELF_MENU_IMAGE_OFF;
    NSString *searchString;
    
    NSMutableString *tmpData;
    NSMutableArray *shelfArray;
    
    SongItem *songItem;
    NSMutableArray *controller;
}

@property (nonatomic, retain) NSString *ID;
@property (nonatomic, retain) NSString *NAMETH;
@property (nonatomic, retain) NSString *NAMEEN;
@property (nonatomic, retain) NSString *DESC;
@property (nonatomic, retain) NSString *TYPE;
@property (nonatomic, retain) NSString *LEVEL;
@property (nonatomic, retain) NSString *SHELF_ID;
@property (nonatomic, retain) NSString *SHELF_ICON;
@property (nonatomic, retain) NSString *SHELF_MENU_IMAGE_ON;
@property (nonatomic, retain) NSString *SHELF_MENU_IMAGE_OFF;
@property (nonatomic, retain) NSString *searchString;

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (retain, nonatomic) SongItem *songItem;

@property (nonatomic, retain) NSMutableArray *controller;

- (void) connectAPI;

@end
