//
//  PlaylistMainController.h
//  Download333
//
//  Created by Aphinop Supawaree on 1/1/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface PlaylistMainController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate *appDelegate;
}

@property (retain, nonatomic) IBOutlet UITableView *tableData;
@property (retain, nonatomic) NSArray *songList;

- (void)listSong;
- (void)removeSongFromPlaylistWithGMMDCode:(NSString *)_gmmdcode;

- (NSString *)getSongFilePath:(NSString *)nameOfFile;


@end
