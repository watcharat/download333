//
//  ShelfLabelItem.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/23/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "ShelfLabelItem.h"

@implementation ShelfLabelItem

@synthesize SHELFLABEL, ID, NAMETH, NAMEEN, DESC, TYPE, LEVEL, SHELF_ID, SHELF_ICON, SHELF_MENU_IMAGE_ON, SHELF_MENU_IMAGE_OFF;

- (void) dealloc {
    [SHELFLABEL release];
    [ID release];
    [NAMETH release];
    [NAMEEN release];
    [DESC release];
    [TYPE release];
    [LEVEL release];
    [SHELF_ID release];
    [SHELF_ICON release];
    [SHELF_MENU_IMAGE_ON release];
    [SHELF_MENU_IMAGE_OFF release];
    [super dealloc];
}

@end
