//
//  LibraryMainController.h
//  Download333
//
//  Created by Aphinop Supawaree on 12/31/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LibraryMainController : UIViewController

@property (retain, nonatomic) IBOutlet UIButton *btnLibrarySong;
@property (retain, nonatomic) IBOutlet UIButton *btnLibraryMV;

- (IBAction)pressToSong:(id)sender;
- (IBAction)pressToMV:(id)sender;


@end
