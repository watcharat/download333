//
//  DownloadItem.m
//  Download333
//
//  Created by Aphinop Supawaree on 12/28/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "DownloadItem.h"
#import "DownloadController.h"
#import "Library.h"

@implementation DownloadItem

@synthesize Content;
@synthesize ContentCode;
@synthesize GMMDCode;
@synthesize sVide;
@synthesize SongNameEN;
@synthesize SongNameTH;
@synthesize thmCover;
@synthesize cover;
@synthesize AlbumTH;
@synthesize ArtistEN;
@synthesize ArtistTH;
@synthesize preview;
@synthesize sDuration;
@synthesize rbt;
@synthesize CP_ID;
@synthesize mvCover;
@synthesize streaming_mv;
@synthesize streaming_fs;
@synthesize lyrics;
@synthesize dlQueue;
@synthesize requestDownload;
@synthesize dl_link;
@synthesize service_id;

NSString *downloadPath;
NSString *tempPath;

- (void) dealloc {
    [Content release];
    [ContentCode release];
    [GMMDCode release];
    [sVide release];
    [SongNameEN release];
    [SongNameTH release];
    [thmCover release];
    [cover release];
    [AlbumTH release];
    [ArtistEN release];
    [ArtistTH release];
    [preview release];
    [sDuration release];
    [rbt release];
    [CP_ID release];
    [mvCover release];
    [streaming_mv release];
    [streaming_fs release];
    [lyrics release];
    [dlQueue release];
    [requestDownload release];
    [dl_link release];
    [service_id release];
}


#pragma mark - Request
- (void)resumeInterruptedDownload
{
    
    NSLog(@">>> GMMDCode : %@", GMMDCode);
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate]; 
    
    if (!self.dlQueue) {
        [self setDlQueue:[[[NSOperationQueue alloc] init] autorelease]];
    }
    
    NSURL *url = [NSURL URLWithString:dl_link];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    
    downloadPath = [self getSongFilePath:GMMDCode];
    tempPath = [NSString stringWithFormat:@"%@.download", downloadPath];
    
    // The full file will be moved here if and when the request completes successfully
    [request setDownloadDestinationPath:downloadPath];
    [request setDelegate:self];
    // This file has part of the download in it already
    [request setTemporaryFileDownloadPath:tempPath];
    [request setNumberOfTimesToRetryOnTimeout:4];
    [request setAllowResumeForFileDownloads:YES];
    
    // iOs 4+ only
    [request setShouldContinueWhenAppEntersBackground:YES];
    //    [request startAsynchronous];
    
    // queue
    [self.dlQueue addOperation:request];
    //    [self.dlQueue setMaxConcurrentOperationCount:1];
    
    requestDownload = request;
    
}

- (NSString *)getSongFilePath:(NSString *)nameOfFile{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *filename;
//    NSLog(@">>> DownloadItem >> service_id : %@",service_id);
    if ([service_id isEqualToString:@"1"]) {
        filename = [[NSString alloc]initWithFormat:@"%@.mp3",nameOfFile];
    }
    else if ([service_id isEqualToString:@"2"]) {
        filename = [[NSString alloc]initWithFormat:@"%@.mp4",nameOfFile];
    }
     
    NSString *uniquePath = [documentDirectory stringByAppendingPathComponent:filename];
    
    return uniquePath;
    
}

//-(void)request:(ASIHTTPRequest *)request didReceiveData:(NSData *)data
//{
//    NSLog(@">>> Download Receive data");
//}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@">>> requestFinished");
//    NSLog(@">>> Res = %@", [request responseString]);
    
//    NSString *plSongMaxId = [NSString stringWithFormat:@"%d",([self getPlSongMaxId]+1)];
    
        
    [self addLibrary];
        
    [[DownloadController sharedInstance].downloadList removeObject:self];
    [[DownloadController sharedInstance].downloadTable reloadData];
        
    
//    [self UpdateBadge];
    
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@">>> Download Failed!");
    NSLog(@">>> Error : %@", [request error]);
}

#pragma mark - ManagedObject
- (void)addLibrary
{
    
    Library *libraryCoreData = [NSEntityDescription insertNewObjectForEntityForName:@"Library" inManagedObjectContext:appDelegate.managedObjectContext];
    
    util = [[MyUtil alloc]init]; 
    libraryCoreData.content = self.Content;
    libraryCoreData.contentCode = self.ContentCode;
    libraryCoreData.album_th = self.AlbumTH;
    libraryCoreData.gmmdcode = self.GMMDCode;
    libraryCoreData.sduration = self.sDuration;
    libraryCoreData.service_id = self.service_id;
    libraryCoreData.songname_en = self.SongNameEN;
    libraryCoreData.songname_th = self.SongNameTH;
    
    if ([self.ArtistTH isEqualToString:@""]) {
        libraryCoreData.artist_name = self.ArtistEN;
    }
    else
    {
        libraryCoreData.artist_name = self.ArtistTH;
    }
    

    NSLog(@"Save Data");
    if ([self.thmCover isEqualToString:@""]) {
        UIImage *imageGmember = [UIImage imageNamed:@"gmember.png"];
        NSData *imgGmemberData = UIImagePNGRepresentation(imageGmember);
        libraryCoreData.cover = imgGmemberData;
        libraryCoreData.thmcover = imgGmemberData;
    }
    else
    {
        libraryCoreData.cover = [util convertImageToNSDataFromURL:self.cover];
        libraryCoreData.thmcover = [util convertImageToNSDataFromURL:self.thmCover];
        libraryCoreData.mvcover = [util convertImageToNSDataFromURL:self.mvCover];
    }
    
    [appDelegate.managedObjectContext save:nil];

    
    
    
    [util release];
    
}

- (BOOL)checkLibraryWithGMMDCode:(NSString *)_gmmdCode andServiceId:(NSString *)_service_id
{
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library"  
											  inManagedObjectContext:appDelegate.managedObjectContext];
	[fetchRequest setEntity:entity];
    
	
    NSString *predicateCondition = [NSString stringWithFormat:@"gmmdcode == %@ AND service_id == %@", _gmmdCode, _service_id];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateCondition];
    [fetchRequest setPredicate:predicate];
    
	NSError *error;
	NSArray *items = [appDelegate.managedObjectContext
					  executeFetchRequest:fetchRequest error:&error];
	
	[fetchRequest release];
	
	if ([items count] > 0) {
		return YES;
	}
	else {
		return NO;
	}
	
}

- (int)getPlSongMaxId
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Library"  
											  inManagedObjectContext:appDelegate.managedObjectContext];
	[fetchRequest setEntity:entity];
    
	NSError *error;
	NSArray *items = [appDelegate.managedObjectContext
					  executeFetchRequest:fetchRequest error:&error];
	
	[fetchRequest release];
	
	return [items count];
}

@end
