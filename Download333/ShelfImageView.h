//
//  ShelfImageView.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/26/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface ShelfImageView : UIImageView

- (void)loadImageFromURL:(NSString*)url;
- (void)thumbnailImage:(NSString*)fileName :(NSString *)defaultImage;

@end
