//
//  SubScribtMember.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/21/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "SubScribtMember.h"

@implementation SubScribtMember

@synthesize STATUS, STATUS_CODE, STATUS_DETAIL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self connectAPI];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //[request release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Connect API
- (void) connectAPI {
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/subscribtMember.jsp?APP_ID=%@&APIVERSION=%@&MSISDN=%@&DEVICE=%@&APPVERSION=%@", APPID, APIVERSION, appDelegate.MSISDN, appDelegate.deviceId, APPVERSION];
    
    NSURL *pathUrl = [NSURL URLWithString:URL];
    NSLog(@"url = %@", pathUrl);
    
    request = [ASIHTTPRequest requestWithURL:pathUrl];
    

    
    [request setDelegate:self];
    [request startSynchronous];
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    NSString *respString = [request responseString];
    NSLog(@"XML = %@", respString);
    NSData *responseData = [respString dataUsingEncoding:NSUTF8StringEncoding];    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:responseData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"error = %@",[request error]);
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {    
    tmpData = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {     
    
    if([elementName isEqualToString:@"STATUS"]) {
        self.STATUS = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"STATUS_CODE"]) {
        self.STATUS_CODE = [NSString stringWithFormat:@"%@", tmpData];
    } else if([elementName isEqualToString:@"STATUS_DETAIL"]) {
        self.STATUS_DETAIL = [NSString stringWithFormat:@"%@", tmpData];
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"STATUS = %@", STATUS);
    NSLog(@"STATUS_CODE = %@", STATUS_CODE);
    NSLog(@"STATUS_DETAIL = %@", STATUS_DETAIL);
    
    if([STATUS isEqualToString:@"OK"] && [STATUS_CODE isEqualToString:@"200"]){
        [appDelegate saveProfileWithMSISDN:appDelegate.MSISDN andCreateDate:[NSDate date]];
        NSLog(@"SAVE DATA SUCESSFUL");
//        [self.view removeFromSuperview];
//        appDelegate.window.rootViewController = appDelegate.mainTab;
    }else{
        [self alertMessageWithTitle:STATUS andMessage:STATUS_DETAIL];
    }
}


#pragma mark - Submit Button Action Control
- (IBAction)btnSubmitTouchUpInside:(id)sender {
    [self.view removeFromSuperview];
    [appDelegate exitView];
}

#pragma mark - Close Button Action Control
- (IBAction)btnCloseButtonTouchUpInside:(id)sender {
    [self.view removeFromSuperview];
    [appDelegate exitView];
}


#pragma mark - Alert Message
- (void) alertMessageWithTitle:(NSString*) title andMessage:(NSString*) message {
    UIAlertView *errView;
    
    errView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@", title] 
                                         message:[NSString stringWithFormat:@"%@", message] 
                                        delegate:self 
                               cancelButtonTitle:@"OK" 
                               otherButtonTitles:nil];
    [errView show];
    [errView release];
}


@end
