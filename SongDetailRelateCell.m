//
//  SongDetailRelateCell.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/4/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "SongDetailRelateCell.h"

@implementation SongDetailRelateCell

@synthesize thumbImage;
@synthesize songName;
@synthesize artistName;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [thumbImage release];
    [songName release];
    [artistName release];
    [super dealloc];
}
@end
