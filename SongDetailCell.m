//
//  SongDetailCell.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/27/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "SongDetailCell.h"

@implementation SongDetailCell

@synthesize coverImage;
@synthesize songName;
@synthesize artistName;
@synthesize albumName;
@synthesize songLikeCount;
@synthesize likeButton;
@synthesize btnPreview;
@synthesize btnTwiterShare;
@synthesize btnFacebookShare;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [coverImage release];
    [songName release];
    [artistName release];
    [albumName release];
    [songLikeCount release];
    [likeButton release];
    [btnPreview release];
    [btnTwiterShare release];
    [btnFacebookShare release];
    [super dealloc];
}


@end


