//
//  MyUtil.h
//  ArtistAppNew
//
//  Created by Aphinop Supawaree on 11/10/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyUtil : NSObject

- (NSData *) convertImageToNSDataFromURL:(NSString *)url;
- (void) cacheImage: (NSString *) ImageURLString andAid:(NSString *)_aid;
- (UIImage *) getCachedImage: (NSString *) ImageURLString andAid:(NSString *)_aid;
- (UIImage *) roundCorners: (UIImage*) img;
- (void) coverPathWithImage: (NSString *) ImageURLString andAid:(NSString *)_aid;
- (UIImage *)getCoverWithAid:(NSString *)_aid;
- (NSString *)getCoverPathWithAid:(NSString *)_aid;

// Date
- (int)DaysOfLastDateToNowDate:(NSDate *)_lastDate;
- (void)OverWriteFileInBundleName:(NSString *)_fileName withFile:(NSData *)_fileData;

@end
