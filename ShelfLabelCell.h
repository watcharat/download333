//
//  ShelfLabelCell.h
//  Download333
//
//  Created by zixboo on 12/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShelfLabelCell : UITableViewCell {
    
    IBOutlet UIImageView *shelfLabelImage;
    
}

@property (nonatomic, retain) UIImageView *shelfLabelImage;

@end
