//
//  AppleMSISDN.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/12/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "ASIHTTPRequest.h"

@interface AppleMSISDN : NSObject <ASIHTTPRequestDelegate> {
    AppDelegate *appDelegate;
}

- (void) getAppleMSISDN;

@end
