//
//  PlayerPlaylistSongCell.m
//  Download333
//
//  Created by Aphinop Supawaree on 1/3/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "PlayerPlaylistSongCell.h"

@implementation PlayerPlaylistSongCell
@synthesize imgSong;
@synthesize lblSongName;
@synthesize lblArtistName;
@synthesize imgBG;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [imgSong release];
    [lblSongName release];
    [lblArtistName release];
    [imgBG release];
    [super dealloc];
}
@end
