//
//  PlayerPlaylistSongCell.h
//  Download333
//
//  Created by Aphinop Supawaree on 1/3/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerPlaylistSongCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imgSong;
@property (retain, nonatomic) IBOutlet UILabel *lblSongName;
@property (retain, nonatomic) IBOutlet UILabel *lblArtistName;
@property (retain, nonatomic) IBOutlet UIImageView *imgBG;

@end
