//
//  SongDetailRelateCell.h
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/4/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SongDetailRelateCell : UITableViewCell {

    IBOutlet UIImageView *thumbImage;
    IBOutlet UILabel *songName;
    IBOutlet UILabel *artistName;
    
}

@property (nonatomic, retain) UIImageView *thumbImage;
@property (nonatomic, retain) UILabel *songName;
@property (nonatomic, retain) UILabel *artistName;

@end
