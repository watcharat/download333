//
//  MyImage.h
//  ArtistApp
//
//  Created by  on 9/1/54 BE.
//  Copyright 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyImage : NSObject
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect;

@end
