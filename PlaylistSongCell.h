//
//  PlaylistSongCell.h
//  Download333
//
//  Created by Aphinop Supawaree on 1/1/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaylistSongCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imgThmCover;
@property (retain, nonatomic) IBOutlet UILabel *lblSongName;
@property (retain, nonatomic) IBOutlet UILabel *lblArtistname;

@end
