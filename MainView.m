//
//  MainView.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 12/20/54 BE.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "MainView.h"
#import "DownloadController.h"

static MainView *sharedSingletonDelegate = nil;

@implementation MainView


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Loading MainView....");
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
}

- (void)viewDidUnload
{
    [btnHilightOfWeek release];
    btnHilightOfWeek = nil;
    [btnSong release];
    btnSong = nil;
    [btnFacebook release];
    btnFacebook = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (void)dealloc {
    [btnHilightOfWeek release];
    [btnSong release];
    [btnFacebook release];
    [super dealloc];
}


- (IBAction)btnHilightOfWeekTouchUpInside:(id)sender {
    [sender setImage:[UIImage imageNamed:@"bt_hi-light_on.png"] forState:UIControlStateNormal];
    [btnSong setImage:[UIImage imageNamed:@"bt_music_off.png"] forState:UIControlStateNormal];
    
    ShelfLabelController *nextController = [[ShelfLabelController alloc] initWithNibName:@"SongListView" bundle:[NSBundle mainBundle]];
    
    nextController.ID = @"1";
    nextController.NAMETH = @"HI-light of week";
    nextController.NAMEEN = @"HI-light of week";
    nextController.DESC = @"desc";
    nextController.TYPE = @"CONTENTS";
    nextController.LEVEL = @"2";
    nextController.SHELF_ID = @"P08002-00050";
    nextController.SHELF_ICON = @"";
    nextController.SHELF_MENU_IMAGE_ON = @"";
    nextController.SHELF_MENU_IMAGE_OFF = @"";

    [appDelegate.mainViewController pushViewController:nextController animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [nextController release];
}


- (IBAction)btnSongTouchUpInside:(id)sender {
    [sender setImage:[UIImage imageNamed:@"bt_music_on.png"] forState:UIControlStateNormal];
    [btnHilightOfWeek setImage:[UIImage imageNamed:@"bt_hi-light_off.png"] forState:UIControlStateNormal];
    
    CategoryListController *nextController = [[CategoryListController alloc] initWithNibName:@"CategoryListView" bundle:[NSBundle mainBundle]];
    
    nextController.shelfID = @"Music";
    
    [appDelegate.mainViewController pushViewController:nextController animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [nextController release];
}


- (IBAction)btnFacebookTouchUpInside:(id)sender {
    [btnSong setImage:[UIImage imageNamed:@"bt_music_on.png"] forState:UIControlStateNormal];
    [btnHilightOfWeek setImage:[UIImage imageNamed:@"bt_hi-light_off.png"] forState:UIControlStateNormal];
    
    FacebookController *fbWebView = [[FacebookController alloc] initWithNibName:@"FacebookView" bundle:[NSBundle mainBundle]];
    [appDelegate.mainViewController pushViewController:fbWebView animated:YES];
    [fbWebView release];
    
}


@end







