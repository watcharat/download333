//
//  AppleMSISDN.m
//  Download333
//
//  Created by อัครพงศ์ วงษาวัตร์ on 1/12/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "AppleMSISDN.h"
#import "AFJSONRequestOperation.h"
#import "AFXMLRequestOperation.h"

@implementation AppleMSISDN

- (id) init {
    if(self = [super init]) {
        appDelegate = (AppleMSISDN *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void) getAppleMSISDN {
    NSString *URL = [NSString stringWithFormat:@"http://hvpiphone.gmember.com/musicapp/api/appleMsisdn.jsp?VERSION=%@", APPVERSION];
    NSLog(@"request url = %@", URL);
    NSURL *pathURL = [NSURL URLWithString:URL];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:pathURL];
    [request setDelegate:self];
    [request startSynchronous];

//    
//    NSURLRequest *request = [NSURLRequest requestWithURL:pathURL];
//
//    AFXMLRequestOperation *operation = [[AFXMLRequestOperation alloc] initWithRequest:request];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSString *respString = [operation responseString];
//        NSLog(@"response: %@",respString);
//        respString = [respString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        if(![respString isEqualToString:@""] && respString.length == 11){
//            NSLog(@"RESPONSE : %@",respString);
//            appDelegate.MSISDN = respString;
//            [appDelegate saveProfileWithMSISDN:appDelegate.MSISDN andCreateDate:[NSDate date]];
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"error: %@", [operation error]);
//    }];
//    
//    [operation start];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    NSString *respString = [request responseString];
    NSLog(@"respString = %@", respString);
    respString = [respString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(![respString isEqualToString:@""] && respString.length == 11){
        appDelegate.MSISDN = respString;
        [appDelegate saveProfileWithMSISDN:appDelegate.MSISDN andCreateDate:[NSDate date]];
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"error = %@",[request error]);
}

@end
